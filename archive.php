<?php 

get_header();

?>

		<main class="main">
            <div class="container-fluid">
                <div class="row justify-content-md-center">
                    <div class="col-md-12 the-banner">
                    <h1 class="title title--xl highlighter">Category: <?php echo single_term_title(); ?></h1>
                    <?php if(category_description()): ?>
                    <div class="" style="padding: 60px 0 0 0; text-align:center; max-width:60%; margin:0 auto;"><?php echo(category_description()); ?></div>
                    <?php endif; ?>
                    <div class="blog-cat-nav"><a href="/blog/">back to all blogs</a></div>
                    <?php if ( has_post_thumbnail() ) { echo '<div class="col-md-12 banner-image" style="background-image: url(' . get_the_post_thumbnail_url(get_the_ID(), 'large' ) . ');"></div>';} else { echo '<div class="col-md-12 banner-spacer"></div>'; }?>
                    </div>
                    
                </div>
                
                    
<div id="ajax-posts" class="row col-md-12">
        
        <?php include locate_template( 'tpl/parts/blog-post-grid.php' ); 
        $category = get_category( get_query_var( 'cat' ) );
        $cat_id = $category->cat_ID; ?>
    
        <?php if ($count > "6") { 
        echo '<div id="more_posts" class="big-link-highlighter" data-type="post" data-category="' .  $cat_id . '">Load More</div>     
        ';} ?>
        
        </div>
        </div>
		</main>

		

<?php get_footer(); ?>
