<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content-wrap div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package twkmedia
 */

?>



</div>
<!-- END DIV MAIN CONTENT -->

<footer id="contact" class="site-footer">
    <div class="container">
		<div class="row">
            <h2 class="title--xl reverse-highlighter">Contact</h2> 
        </div>
    </div>
	<div class="container">
		<div class="row">
             <div class="col-md-12 footer-socials">
						<?php
						if ( has_nav_menu( 'socials_menu' ) ) {
							wp_nav_menu(
								array(
									'theme_location' => 'socials_menu',
									'menu_id'        => 'socials_menu',
								)
							);
						}
						?>
					</div>
		</div>
	</div>

	<div class="copyright mt-4 ">
		<div class="container">
			<div class="row">
				<div class="col col-md-6 text-left color-primary d-flex align-items-center">
				© <?php echo get_bloginfo( 'name' ); ?> <?php echo date( 'Y' ); ?>
				</div>

                <div class="col-md-6 text-right footer-nav">
						<?php
						if ( has_nav_menu( 'footer_menu' ) ) {
							wp_nav_menu(
								array(
									'theme_location' => 'footer_menu',
									'menu_id'        => 'footer_menu',
								)
							);
						}
						?>
					</div>
                
				<!-- <div class="col text-left text-md-right color-primary">
					<p class="d-inline-block mb-0"><a href="#" target="_blank" rel="external" class="color-inherit">Web design</a></p>
					<p class="d-inline-block mb-0">by</p>
					<p class="d-inline-block mb-0"><a href="#" target="_blank" rel="external" class="color-inherit">Martin J Stephens</a></p>
				</div> -->
			</div>
		</div>
	</div>
</footer>

</div>

<!-- JS ARE LOADED HERE WITH FUNCTION.PHP -->
<?php wp_footer(); ?>


</body>

</html>
