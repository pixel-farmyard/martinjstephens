<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content-wrap">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package twkmedia
 */

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head>

	<!-- Start cookieyes banner -->
	<script id="cookieyes" type="text/javascript" src="https://cdn-cookieyes.com/client_data/900b2ab50f2702b98d2f6bb1/script.js"></script>
	
	<!-- Google tag (gtag.js) -->
	<script data-cookieyes="cookieyes-analytics" async src="https://www.googletagmanager.com/gtag/js?id=G-57V22G18T9"></script>
	<script data-cookieyes="cookieyes-analytics">
  	window.dataLayer = window.dataLayer || [];
  	function gtag(){dataLayer.push(arguments);}
  	gtag('js', new Date());

  	gtag('config', 'G-57V22G18T9');
	</script>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php if ( is_search() ) : ?>
	<meta name="robots" content="noindex, nofollow" />
	<?php endif; ?>

	<!-- USE YOAST TO ADD SOCIAL MEDIA META TAGS -->

	<!-- GENERATE FAVICON USING https://realfavicongenerator.net/ -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    
    <!-- Adobe font -->
    <link rel="stylesheet" href="https://use.typekit.net/uka8zbq.css">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php if ( strpos( home_url(), 'martinjstephens.com' ) == false ) : ?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-XXXXXXX"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php endif; ?>

	<?php
	// The blog index page needs to get the queried ID
	// and not the first post ID.
	global $post;
	$post = get_queried_object_id();

	edit_post_link( 'Edit page' );
	?>

	<div id="page-wrap">
        <div id="menuToggle">
                            <input type="checkbox" />
                            <span></span>
                            <span></span>
                            <span></span>
		<div class="top-nav">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 text-right">
                        
						<?php
						if ( has_nav_menu( 'main_menu' ) ) {
							wp_nav_menu(
								array(
									'theme_location' => 'main_menu',
									'menu_id'        => 'main-menu',
								)
							);
						}
						?>
					</div>
				</div>
			</div>
		</div>
        
                            
        </div>
		<div id="content-wrap">
