<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * @package twkmedia
 * @version  1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();
?>

	<div id="ajax-cal-content">
		<div class="cal-content">
			<h3 class="event-title"><?php echo get_the_title(); ?></h3>

			<div class="cal-event-time">
				<?php if ( ! tribe_event_is_all_day() && tribe_get_start_date( $event_id, false, 'H:i' ) !== '00:00' ) : ?>

					<span class="time-event">
						<?php echo tribe_get_start_date( $event_id, false, 'F d | H:i' ); ?> 
						<?php if ( tribe_get_end_date( $event_id, false, 'H:i') !== '00:00' ): ?>
							- <?php echo tribe_get_end_date( $event_id, false, 'H:i' ); ?>
						<?php endif; ?>
					</span>

				<?php else : ?>

					<span class="time-event">
						<?php if ( tribe_get_start_date( $event_id, false, 'H:i' ) !== '00:00' ) : ?>
							<?php echo tribe_get_start_date( $event_id, false, 'F d | H:i' ); ?> 
						<?php else : ?>
							<?php echo tribe_get_start_date( $event_id, false, 'F d' ); ?> 
						<?php endif; ?>
					</span>

				<?php endif; ?>
			</div>

			<?php if ( get_the_content() ) : ?>
				<div class="cal-description">
					<?php echo get_the_content(); ?>
				</div>
			<?php endif; ?>

			<?php do_action( 'tribe_events_single_event_after_the_content' ); ?>
		</div>
	</div>

