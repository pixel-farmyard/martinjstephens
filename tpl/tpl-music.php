<?php
/**
 * Template Name: Music
 */

get_header();

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>

		<main class="main">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-12 the-banner">
                    <h1 class="title title--xl highlighter"><?php echo get_the_title(); ?></h1>
                    <?php $introtext = get_field('intro');
                        if ( !empty( $introtext ) ) {echo '<p class="page-intro">' . $introtext . '</p>';}
                        if ( has_post_thumbnail() ) { echo '<div class="col-md-12 banner-image" style="background-image: url(' . get_the_post_thumbnail_url(get_the_ID(), 'large' ) . ');"></div>';} 
                        if ( empty($introtext) && empty(has_post_thumbnail()) )  { echo '<div class="col-md-12 banner-spacer"></div>'; }?>
                    </div>
                    
                </div>
                <!-- First video -->
                    <div class="row justify-content-md-center">    
                        <div class="video-tile col-lg-12 col-md-12 featured-video music-video" style="background:url(<?php echo get_field('video_placeholder_1'); ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                                <video class="video-preview" preload="auto" muted playsinline loop autoplay>
                                    <source src="<?php echo get_field('video_bg_1'); ?>" type="video/mp4">
                                </video>
                            <a class="magnific-video" href="<?php echo get_field( "video_url_1" ); ?>"><img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" /></a>
                        <a class="magnific-video video-link" href="<?php echo get_field( "video_url_1" ); ?>"></a>
                                    <div class="video-tile-text">
                                    <h3><?php echo get_field('video_title_1'); ?></h3>
                                    <p class="video-excerpt"><?php echo get_field( "video_description_1"); ?></p>
                                    </div>
                        </div>
                </div>
            </div>
            <!-- Gallery -->
                <div class="container-fluid image-gallery">
                <div class="row image-gallery-inner no-gutters">
                    <div class="row gallery-level-one col-lg-12 no-gutters">
                        <div class="gallery-image-1 col-lg-5" style="background-image:url(<?php echo get_field('gallery_image_1'); ?>);">
                            <p class="gallery-excerpt"><span><?php echo get_field( "gallery_caption_1"); ?></span></p>
                        </div>
                        <div class="gallery-level-two col-lg-4 no-gutters">
                            <div class="gallery-image-2" style="background-image:url(<?php echo get_field('gallery_image_2'); ?>);">
                            <p class="gallery-excerpt"><span><?php echo get_field( "gallery_caption_2"); ?></span></p>
                            </div>  
                            <div class="gallery-image-3" style="background-image:url(<?php echo get_field('gallery_image_3'); ?>);">
                            <p class="gallery-excerpt"><span><?php echo get_field( "gallery_caption_3"); ?></span></p>
                            </div>  
                            <div class="gallery-image-4" style="background-image:url(<?php echo get_field('gallery_image_4'); ?>);">
                            <p class="gallery-excerpt"><span><?php echo get_field( "gallery_caption_4"); ?></span></p>
                            </div>  
                            <div class="gallery-image-5" style="background-image:url(<?php echo get_field('gallery_image_5'); ?>);">
                            <p class="gallery-excerpt"><span><?php echo get_field( "gallery_caption_5"); ?></span></p>
                            </div>  
                        </div>
                        <div class="gallery-image-6 col-lg-3" style="background-image:url(<?php echo get_field('gallery_image_6'); ?>);">
                            <p class="gallery-excerpt"><span><?php echo get_field( "gallery_caption_6"); ?></span></p>
                        </div>
                    </div>
                    
                </div>
                </div>
                <!-- Performance Links -->
            <div class="container">
                <div id="performance-credits">
                    <div class="container">
                        <div class="row">
                            <h2 class="title--xl highlighter"><?php echo get_field('performance_title');?></h2>
                            <p class="page-intro"><?php echo get_field('performance_intro');?></p>
                        </div>
                        <div class="row justify-content-md-center">
                            <a class="magnific-video col-md-6 performance-credit-link" href="<?php echo get_field('credit_link_1') ?>">
                            
                            <div class="performance-tile" style="background:url(<?php echo get_field('credit_image_1') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                                <img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" />
                            </div>
                            <h3><?php echo get_field('credit_title_1') ?></h3>
                            <p><?php echo get_field('credit_info_1') ?></p>
                            </a>
                            
                            <a class="magnific-video col-md-6 performance-credit-link" href="<?php echo get_field('credit_link_2') ?>">
                            
                            <div class="performance-tile" style="background:url(<?php echo get_field('credit_image_2') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                                <img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" />
                            </div>
                            <h3><?php echo get_field('credit_title_2') ?></h3>
                            <p><?php echo get_field('credit_info_2') ?></p>
                            </a>
                            
                            <a class="magnific-video col-md-6 performance-credit-link" href="<?php echo get_field('credit_link_3') ?>">
                            
                            <div class="performance-tile" style="background:url(<?php echo get_field('credit_image_3') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                                <img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" />
                            </div>
                            
                            <h3><?php echo get_field('credit_title_3') ?></h3>
                            <p><?php echo get_field('credit_info_3') ?></p>
                            </a>
                            
                            
                            <a class="magnific-video col-md-6 performance-credit-link" href="<?php echo get_field('credit_link_4') ?>">
                            
                            <div class="performance-tile" style="background:url(<?php echo get_field('credit_image_4') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                                <img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" />
                            </div>
                            <h3><?php echo get_field('credit_title_4') ?></h3>
                            <p><?php echo get_field('credit_info_4') ?></p>
                            </a>
                        
                        </div>
                    </div>
                </div>
                <!-- Second video -->
                <div class="row justify-content-md-center">    
                        <div class="video-tile col-lg-12 col-md-12 featured-video music-video" style="background:url(<?php echo get_field('video_placeholder_2'); ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                            <video class="video-preview" preload="auto" muted playsinline loop autoplay>
                                    <source src="<?php echo get_field('video_bg_2'); ?>" type="video/mp4">
                                </video>
                            <a class="magnific-video" href="<?php echo get_field( "video_url_2" ); ?>"><img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" /></a>
                        <a class="magnific-video video-link" href="<?php echo get_field( "video_url_2" ); ?>"></a>
                                    <div class="video-tile-text">
                                    <h3><?php echo get_field('video_title_2'); ?></h3>
                                    <p class="video-excerpt"><?php echo get_field( "video_description_2"); ?></p>
                                    </div>
                        </div>
                </div> 
                
                <?php $outro = get_field('outro_text');
                        if ( !empty( $outro ) ) {include locate_template( 'tpl/parts/outro.php' );}
                ?>
                
            </div>
		</main>

		<?php
	endwhile;
endif;

get_footer();
