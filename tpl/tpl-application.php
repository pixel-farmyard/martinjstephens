<?php
/**
 * Template Name: Application
 */

get_header();

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();

        $companyname = get_field('company_name');
        $companylogo = get_field('company_logo');

		?>

		<main class="main">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-12 the-banner">
                    <img class="company-logo" src="<?php echo $companylogo; ?>" />
                    <h1 class="title title--xl highlighter"><?php 
                        if ( !empty ($companyname)) {echo 'Hello there, ' . $companyname;}
                        if ( empty ($companyname)) {echo get_field('title_override'); }
                    ?></h1>
                    <?php $introtext = get_field('intro');
                        if ( !empty( $introtext ) ) {echo '<p class="page-intro">' . $introtext . '</p>';}
                        if ( has_post_thumbnail() ) { echo '<div class="col-md-12 banner-image" style="background-image: url(' . get_the_post_thumbnail_url(get_the_ID(), 'large' ) . ');"></div>';} 
                        if ( empty($introtext) && empty(has_post_thumbnail()) )  { echo '<div class="col-md-12 banner-spacer"></div>'; }?>
                    </div>
                </div>
                
                <div class="row justify-content-md-center application-video">    
                        <div class="video-tile col-lg-12 col-md-12 featured-video music-video" style="background:url(<?php echo get_field('video_placeholder'); ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                            <video class="video-preview" preload="auto" muted playsinline loop autoplay>
                                    <source src="<?php echo get_field('video_bg'); ?>" type="video/mp4">
                                </video>
                            <a class="magnific-video" href="<?php echo get_field( "video_url" ); ?>"><img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" /></a>
                        <a class="magnific-video video-link" href="<?php echo get_field( "video_url" ); ?>"></a>
                                    <div class="video-tile-text">
                                    <h3><?php echo get_field('video_title'); ?></h3>
                                    <p class="video-excerpt"><?php echo get_field( "video_description"); ?></p>
                                    </div>
                        </div>
                </div> 
                
                <div class="row justify-content-md-center about-company">
                    <div class="col-md-8">
                        <h3><?php 
                        if ( !empty ($companyname)) {echo 'Why ' . $companyname . '?';}
                        if ( empty ($companyname)) {echo 'Why your company?'; }
                        ?></h3>
                        <img class="company-logo" src="<?php echo $companylogo; ?>" />
                    </div>
                </div>
                <div class="row justify-content-md-center about-company">
                    <div class="the-content acf-wysiwyg col-md-8">
                    
                    <?php $content = get_field('about_company');
                        echo $content; 
                    ?>
                    </div>
                </div>
            </div>
                
            <div class="container-fluid employment">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title--xl highlighter"><?php 
                        if ( !empty ($companyname)) {echo 'Three things ' . $companyname . ' does well';}
                        if ( empty ($companyname)) {echo 'Three things you do well'; }
                    ?></h2> 
                            <p class="page-intro"><?php echo get_field('company_intro');?></p>
                        </div>
                    </div>
                <div class="row justify-content-start employment-history-two" style="background:url(<?php echo get_field('company_image_1') ?>);
                                                    background-position:top right;
                                                    background-size:contain;
                                                    background-repeat:no-repeat">
                        <div class="col-lg-7 col-md-8 offset-md-1 employer-wrapper" >
                            <h3 class="reverse-highlighter"><?php echo get_field('company_title_1');?></h3>
                            <?php $content = get_field('company_text_1');
                                echo $content; 
                            ?>
                            
                        </div>
                        
                </div>
                <div class="row justify-content-end employment-history-two" style="background:url(<?php echo get_field('company_image_2') ?>);
                                                    background-position:top left;
                                                    background-size:contain;
                                                    background-repeat:no-repeat">
                        <div class="col-lg-7 col-md-8 offset-md-1 employer-wrapper" >
                            <h3 class="reverse-highlighter"><?php echo get_field('company_title_2');?></h3>
                            <?php $content = get_field('company_text_2');
                                echo $content; 
                            ?>
                            
                        </div>
                        
                </div>
                <div class="row justify-content-start employment-history-two" style="background:url(<?php echo get_field('company_image_3') ?>);
                                                    background-position:top right;
                                                    background-size:contain;
                                                    background-repeat:no-repeat">
                        <div class="col-lg-7 col-md-8 offset-md-1 employer-wrapper" >
                            <h3 class="reverse-highlighter"><?php echo get_field('company_title_3');?></h3>
                            <?php $content = get_field('company_text_3');
                                echo $content; 
                            ?>
                            
                        </div>
                        
                </div>
            
            </div>
            
            
            <div class="container">    
                <div class="row">
                        <div class="col-md-12">
                            <h2 class="title--xl highlighter"><?php 
                        if ( !empty ($companyname)) {echo 'Why I&apos;m a great fit for ' . $companyname;}
                        if ( empty ($companyname)) {echo 'Why I&apos;m a great fit for your company'; }
                    ?></h2> 
                        </div>
                    </div>
                <div class="row justify-content-md-center">
                    <div style="margin-top:4vh;" class="the-content acf-wysiwyg col-md-8">
                    <?php $content = get_field('about_me');
                        echo $content; 
                    ?>
                    </div>
                </div>
                
            
            </div>
                

    <div class="container-fluid development-wrapper">
                <div class="row">
                        <div class="col-md-12">
                            <h2 class="title--xl highlighter"><?php 
                        if ( !empty ($companyname)) {echo 'My development with ' . $companyname;}
                        if ( empty ($companyname)) {echo 'My development with you'; }
                    ?></h2> 
                            <p class="page-intro"><?php echo get_field('development_intro');?></p>
                        </div>
                </div>
            <div class="row justify-content-center ">
                <div class="col-md-4">
                    <div class="education-wrapper">
                            <div class="education-image" style="background:url(<?php echo get_field('task_image_1') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                            </div>
                            <h3><?php echo get_field('task_name_1') ?></h3>
                            <?php $content = get_field('task_text_1');
                                echo $content; 
                            ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="education-wrapper">
                            <div class="education-image" style="background:url(<?php echo get_field('task_image_2') ?>);
                                                     background-position:center;
                                                    background-size:cover;">
                            </div>
                            <h3><?php echo get_field('task_name_2') ?></h3>
                            <?php $content = get_field('task_text_2');
                                echo $content; 
                            ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="education-wrapper">
                            <div class="education-image" style="background:url(<?php echo get_field('task_image_3') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                            </div>
                            <h3><?php echo get_field('task_name_3') ?></h3>
                            <?php $content = get_field('task_text_3');
                                echo $content; 
                            ?>
                    </div>
                </div>
        </div>
    </div>

            
<div class="container">

                
                <?php $outro = get_field('outro_text');
                        if ( !empty( $outro ) ) {include locate_template( 'tpl/parts/outro.php' );}
                ?>
                
            </div>
		</main>

		<?php
	endwhile;
endif;

get_footer();
