<?php
/**
 * Template Name: CV
 */

get_header();

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>

		<main class="main">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-12 the-banner">
                    <h1 class="title title--xl highlighter"><?php echo get_the_title(); ?></h1>
                    <?php $introtext = get_field('intro');
                        if ( !empty( $introtext ) ) {echo '<p class="page-intro">' . $introtext . '</p>';}
                        if ( has_post_thumbnail() ) { echo '<div class="col-md-12 banner-image" style="background-image: url(' . get_the_post_thumbnail_url(get_the_ID(), 'large' ) . ');"></div>';} 
                        if ( empty($introtext) && empty(has_post_thumbnail()) )  { echo '<div class="col-md-12 banner-spacer"></div>'; }?>
                    </div>
                </div>
                <div class="row justify-content-center tagline-descriptions">
                    <div class="col-md-4">
                        <div class="tagline-description">
                        <h2 class="title--xl">🎸</h2>
                        <h3 class="highlighter">musician</h3>
                        <p><?php echo get_field('musician_text');?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tagline-description">
                        <h2 class="title--xl">🖥</h2>
                        <h3 class="highlighter">web developer</h3>
                        <p><?php echo get_field('web_guy_text');?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tagline-description">
                        <h2 class="title--xl">🐄</h2>
                        <h3 class="highlighter">farmer</h3>
                        <p><?php echo get_field('farmer_text');?></p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="the-content acf-wysiwyg col-md-8 profile-content">
                    <?php $content = get_field('profile');
                        echo $content; 
                    ?>
                    </div>
                </div>
            </div>
                
            <div class="container-fluid employment">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title--xl highlighter">Employment history</h2> 
                        </div>
                    </div>


                    <?php if(have_rows('employment_history')): 
                        while(have_rows('employment_history')) : the_row(); ?>

                        <div class="row justify-content-center employment-history-current" style="background:url(<?php echo get_sub_field('background_image') ?>);
                                                        background-position:top left;
                                                        background-size:cover;
                                                        background-repeat:no-repeat">
                            <div class="col-lg-8 col-md-12 employer-wrapper" >
                                <h3 class="reverse-highlighter"><?php echo get_sub_field('employer_name');?></h3>
                                <p class="green-me job-title"><?php echo get_sub_field('position');?></p>
                                <p class="green-me job-location"><?php echo get_sub_field('location');?></p>
                                <?php $content = get_sub_field('employer_content');
                                    echo $content; 
                                    
                                    $companylogo = get_sub_field('logo');
                                ?>
                                <img class="employer-logo" width="80" height="80" alt="<?php echo($companylogo['alt']);?>" src="<?php echo($companylogo['url']);?>" />
                                <?php if(have_rows('client_logos')): ?>
                                    <p class="green-me clients-included" style="margin-top:50px;">clients included...</p>
                                    <div class="client-logos">
                                        <?php while(have_rows('client_logos')): the_row(); 
                                            $clientlogo = get_sub_field('client_logo'); ?>

                                            <img class="client-logo" width="120" height="60" alt="<?php echo($clientlogo['alt']); ?>" src="<?php echo($clientlogo['url']);?>" />
                                        
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>
                                <?php $employerlink = get_sub_field('link');
                            if ( $employerlink ) {echo '<a class="link-highlighter" target="' . esc_attr($employerlink['target']) . '" href="' . esc_url($employerlink['url']) . '">' . $employerlink['title'] . '</a>';} ?>
                            </div>
                        </div>

                    <?php endwhile; endif; ?>
                        
                </div>
                
                
            
            </div>
            
            
            <div class="container">  

            <?php if(have_rows('skills')): ?>
                
            <div class="row skills">
                        <h2 class="title--xl highlighter">Skills and expertise</h2>
                </div>
                
                <div class="row justify-content-md-center skills">
                    <?php while(have_rows('skills')): the_row(); ?>
                    <div class="col-md-6">
                        <div class="skill-wrapper">
                            <div class="skill-image" style="background:url(<?php echo get_sub_field('image') ?>);
                                                        background-position:center;
                                                        background-size:cover;">
                            </div> 
                        <h3><?php echo get_sub_field('title') ?></h3>
                        <p><?php echo get_sub_field('text') ?></p>
                        </div>
                    </div>

                    <?php endwhile; ?>

                </div>

                <?php endif; ?>


                <div class="row justify-content-center other-work">
                            <h2 class="title--xl reverse-highlighter">Other work</h2>
                            <p class="page-intro"><?php echo get_field('other_work_intro');?></p>
                        </div>
                <div class="row justify-content-center other-work">
                    <div class="col-md-5">
                            <div class="performance-tile" style="background:url(<?php echo get_field('other_work_image_1') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                            </div>
                            <h3 class="reverse-highlighter"><?php echo get_field('other_work_title_1') ?></h3>
                            <p><?php echo get_field('other_work_text_1') ?></p>
                    </div>
                    <div class="col-md-5">
                            <div class="performance-tile" style="background:url(<?php echo get_field('other_work_image_2') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                            </div>
                            <h3 class="reverse-highlighter"><?php echo get_field('other_work_title_2') ?></h3>
                            <p><?php echo get_field('other_work_text_2') ?></p>
                    </div>
                </div>
                
                
            </div>
                
    <?php if(have_rows('education')): ?>
    <div class="container-fluid education">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2 class="title--xl">Education</h2> 
            </div>
        </div>
            <div class="row justify-content-center ">
                <?php while(have_rows('education')) : the_row(); ?>
                <div class="col-md-4">
                    <div class="education-wrapper">
                            <div class="education-image" style="background:url(<?php echo get_sub_field('image') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                            </div>
                            <h3><a href="<?php echo get_sub_field('link')['url']; ?>" target="_blank"><?php echo get_sub_field('title'); ?></a></h3>
                            <span class="green-me"><?php echo get_sub_field('years'); ?></span>
                            <p><?php echo get_sub_field('text') ?></p>
                    </div>
                </div>
                
                <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>

            
<div class="container">

                
                <div class="row justify-content-md-center">
                    <div class="the-content acf-wysiwyg col-md-8">
                    <?php $content = get_field('interests');
                        echo $content; 
                    ?>
                    </div>
                </div>
                
                <?php $outro = get_field('outro_text');
                        if ( !empty( $outro ) ) {include locate_template( 'tpl/parts/outro.php' );}
                ?>
                
            </div>
		</main>

		<?php
	endwhile;
endif;

get_footer();
