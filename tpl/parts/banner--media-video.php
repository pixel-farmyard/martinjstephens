<?php
/**
 * Template part for displaying the page banner media video.
 *
 * @package twkmedia
 */

$detect    = new Mobile_Detect;
$image_url = twk_get_image_url_with_fallback( $post->ID, 'full' );
?>

<div class="banner banner--media d-flex align-items-<?php echo get_field( 'banner_content_position' ); ?>
		<?php echo ( get_field( 'banner_full_height' ) ? 'banner--full-height' : '' ); ?> img-background"
	>

	<?php $poster = get_field( 'banner_image' ); ?>

	<div class="video">
		<?php
		if ( get_field( 'video_type' ) === 'file' ) :
			if ( $detect->isMobile() && get_field( 'banner_video_mobile' ) ) {
				$video_url  = get_field( 'banner_video_mobile' )['url'];
				$video_mime = get_field( 'banner_video_mobile' )['mime_type'];
			} else {
				if ( get_field( 'banner_video_desktop' ) ) {
					$video_url  = get_field( 'banner_video_desktop' )['url'];
					$video_mime = get_field( 'banner_video_desktop' )['mime_type'];
				}
			}
		elseif ( get_field( 'video_type' ) === 'url' ) :
			$video_url  = get_field( 'banner_video_url' );
			$video_mime = 'video/mp4';
		endif;
		?>

		<video poster="<?php echo $poster['url']; ?>" autoplay muted loop>	
			<source src="<?php echo $video_url; ?>" type="<?php echo $video_mime; ?>">
			Your browser does not support HTML5 video.
		</video>
	</div>

	<div class="container overlay--content-over text-center">
		<div class="row justify-content-center">
			<div class="col-md-8 py-15 off-screen off-screen--hide off-screen--fade-up">

				<h1 class="banner__title text-white">
					<?php if ( get_field( 'banner_alternative_title' ) ) : ?>
						<?php echo get_field( 'banner_alternative_title' ); ?>
					<?php else : ?>
						<?php the_title(); ?>
					<?php endif; ?>
				</h1>

				<?php if ( get_field( 'banner_intro_text' ) ) : ?>
					<p class="banner__intro text-white"><?php echo get_field( 'banner_intro_text' ); ?></p>
				<?php endif; ?>

				<?php if ( get_post_type() === 'post' ) : ?>
					<p class="banner__date"><?php echo get_the_date(); ?></p>
				<?php endif; ?>

			</div>
		</div>
	</div>

	<!-- Overlays -->
	<?php $overlay = get_field('banner_overlay'); ?>

	<?php if ( in_array( 'top', $overlay ) ) : ?>
		<div class="overlay overlay--top"></div>
	<?php endif; ?>

	<?php if ( in_array( 'bottom', $overlay ) ) : ?>
		<div class="overlay overlay--bottom"></div>
	<?php endif; ?>

	<?php if ( in_array( 'full', $overlay ) ) : ?>
		<div class="overlay overlay--full"></div>
	<?php endif; ?>
	<!-- END Overlays -->
</div>
