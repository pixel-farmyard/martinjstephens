<?php
/**
 * Template part for displaying the page banner
 *
 * @package twkmedia
 */

?>

<div class="banner py-30">
	<div class="container text-center">
		<div class="row justify-content-center">
			<div class="col-md-8 off-screen off-screen--hide off-screen--fade-up">

				<h1 class="banner__title">
					<?php if ( get_field( 'banner_alternative_title' ) ) : ?>
						<?php echo get_field( 'banner_alternative_title' ); ?>
					<?php else : ?>
						<?php the_title(); ?>
					<?php endif; ?>
				</h1>

				<?php if ( get_field( 'banner_intro_text' ) ) : ?>
					<p class="banner__intro"><?php echo get_field( 'banner_intro_text' ); ?></p>
				<?php endif; ?>

				<?php if ( get_post_type() === 'post' ) : ?>
					<p class="banner__date"><?php echo get_the_date(); ?></p>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>

