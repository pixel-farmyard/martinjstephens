<?php
/**
 * Template part to display a notification popup
 *
 * @package twkmedia
 */

// Cookies.
$cookie_name = get_field( 'popup_cookies_name', 'option' );
if ( '' === $cookie_name ) {
	$cookie_name = 'twk_cookies_popup_00001';
}
?>

<?php if ( get_field( 'popup_active', 'option' ) ) : ?>
	<div class="notification popup" style="display: none;">
		<div class="notification__content text-white text-center">
			<?php if ( get_field( 'popup_text', 'option' ) ) : ?>
				<div class="notification__text"><?php the_field( 'popup_text', 'option' ); ?></div>
			<?php endif; ?>

			<?php
			if ( get_field( 'popup_link', 'option' ) ) :
				echo twk_compose_acf_link( get_field( 'popup_link', 'option' ), 'button' );
			endif;
			?>
		</div>

		<div class="close-button">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				<g fill="none" stroke="#fff" stroke-linecap="square">
					<path d="M0.5,0.5 L23.127417,23.127417"/>
					<path d="M0.5,0.5 L23.127417,23.127417" transform="matrix(-1 0 0 1 23.627 0)"/>
				</g>
			</svg>
		</div>
	</div>
	<?php
endif;
?>



<script>
	function defer(method) {
		if (window.jQuery) {
			method();
		} else {
			setTimeout(function() { defer(method) }, 50);
		}
	}
	defer(function () {
		(function ($){

			$(window).on('load', function() {
				function getCookie(name) {
					var dc = document.cookie;
					var prefix = name + "=";
					var begin = dc.indexOf("; " + prefix);
					if (begin == -1) {
						begin = dc.indexOf(prefix);
						if (begin != 0) return null;
					}
					else
					{
						begin += 2;
						var end = document.cookie.indexOf(";", begin);
						if (end == -1) {
						end = dc.length;
						}
					}
					// because unescape has been deprecated, replaced with decodeURI
					//return unescape(dc.substring(begin + prefix.length, end));
					return decodeURI(dc.substring(begin + prefix.length, end));
				}


				let cookie_name                       = "<?php echo esc_html( $cookie_name ); ?>";
				let popup_notification_cookies_name   = getCookie(cookie_name);				 // Checks if the message was already displayed and closed.
				let twk_cookies_marketing             = getCookie('twk_cookies_marketing');  // Checks if the user accepted "Marketing" cookies.
				let cookie_expire                     = "<?php echo get_field( 'popup_expiry_time', 'option' ); ?>";


				if ( ! popup_notification_cookies_name && twk_cookies_marketing ){
					$( '.notification.popup' ).fadeIn();
				}
				// When they user accepted the cookies.
				$('.cookies-bar .button-accept').on('click', function(){
					$( '.notification.popup' ).fadeIn();
				});

				// Accepts cookies.
				$('.notification.popup .close-button').on('click', function(event){
					event.preventDefault();

					setCookie( cookie_name, 'true', cookie_expire);

					$( ".notification.popup" ).fadeTo("slow", 0, function(){
						$(".notification.popup").css('pointer-events', 'none');
					});

				});
				// Accepts cookies and go to link.
				$('.notification.popup .button').on('click', function(event){
					event.preventDefault();

					setCookie( cookie_name, 'true', cookie_expire);

					window.location = $(this).attr("href");

				});

				function setCookie(cname, cvalue, exdays) {
					var d = new Date();
					d.setTime(d.getTime() + (exdays*24*60*60*1000));
					var expires = "expires="+ d.toUTCString();

					if ( exdays == 0 ){
						document.cookie = cname + "=" + cvalue + ";path=/";
					} else {
						document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
					}
				}

			});

		})(jQuery);
	});
</script>
