<?php
            $postsPerPage = 6;
            $category = get_category( get_query_var( 'cat' ) );
            $cat_id = $category->cat_ID;
            $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => $postsPerPage,
                    'post_status' => array('publish'),
                    'cat' => $cat_id,
            );
            
            $loop = new WP_Query($args);
            $count = $loop->found_posts;
            while ($loop->have_posts()) : $loop->the_post();
    
        ?>

         <a class="col-lg-6 col-md-12 col-sm-12 blog-tile-link" href="<?php the_permalink() ?>">
                        <div class="blog-tile" style="background:url(<?php echo the_post_thumbnail_url( 'medium' ); ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                        <span class="post-categories post-meta"><?php $categories = get_the_category();
                                                        if ( ! empty( $categories ) ) {
                                                        echo esc_html( $categories[0]->name );   
                                                    } ?></span>
                <span class="post-date post-meta"><?php echo get_the_date( 'd M Y', $post->ID ); ?></span>
                <div class="blog-tile-text">
                <h3><?php the_title(); ?></h3>
                <p class="post-excerpt"><?php echo get_the_excerpt(); ?></p>
                
                </div>
            </div>
            </a>
         <?php
        
                endwhile;
        wp_reset_postdata();
?>