<?php
/**
 * Template part to display the pagination.
 *
 * @package suttontrust
 */

if ( ! isset( $query ) && isset( $the_query ) ) {
	$query = $the_query;
}
if ( ! isset( $query ) ) {
	$query = $wp_query;
}

if ( isset( $query ) ) :
	$links = paginate_links(
		array(
			'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			'total'        => $query->max_num_pages,
			'current'      => max( 1, get_query_var( 'paged' ) ),
			'format'       => '?paged=%#%',
			'show_all'     => false,
			'type'         => 'array',
			'end_size'     => 2,
			'mid_size'     => 1,
			'prev_next'    => true,
			'prev_text'    => sprintf( '%1$s', __( 'Previous', 'suttontrust' ) ),
			'next_text'    => sprintf( '%1$s', __( 'Next', 'suttontrust' ) ),
			'add_args'     => false,
			'add_fragment' => '',
		)
	);

	if ( isset( $links ) ) :
		$has_prev = false;
		$has_next = false;

		if ( strpos( $links[0], 'prev' ) !== false ) {
			$has_prev = true;
		}

		if ( strpos( end( $links ), 'next' ) !== false ) {
			$has_next = true;
		}
		?>

		<div class="row">
			<div class="col pagination d-flex justify-content-between align-items-center">

				<?php
				// Prev link.
				if ( isset( $links[0] ) && $has_prev ) {
					echo $links[0];
				} else {
					echo '<div class="placeholder"></div>';  // It needs a placeholder div to the flex property work as intended on design.
				}
				?>

				<div class="pagination__pages">
					<?php
					foreach ( $links as $key => $link ) {
						if ( ( $key !== 0 || ! $has_prev ) && ( $key !== count( $links ) - 1 || ! $has_next ) ) {
							echo $link;
						}
					}
					?>
				</div>

				<?php
				// Next link.
				if ( $has_next ) {
					echo end( $links );
				} else {
					echo '<div class="placeholder"></div>';  // It needs a placeholder div to the flex property work as intended on design.
				}
				?>

			</div>
		</div>
		<?php
	endif;
endif;
