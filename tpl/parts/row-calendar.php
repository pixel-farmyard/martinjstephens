<?php
/**
 * Template for a table row.
 *
 * @package twkmedia
 */

?>

<tr>
	<td class="event">
		<a href="<?php echo esc_url( get_the_permalink( $ev_id ) ); ?>" title="<?php echo get_the_title( $ev_id ); ?>" data-id="<?php echo esc_attr( $ev_id ); ?>">
			<?php echo get_the_title( $ev_id ) . ' »'; ?>
		</a>
	</td>

	<td class="ta_c">
		<?php if ( ! tribe_event_is_all_day( $ev_id ) ) : ?>
			<?php if ( tribe_get_start_date( $ev_id, false, 'H:i' ) !== '00:00' ) : ?>

				<span class="time-event">
					<?php echo tribe_get_start_date( $ev_id, false, 'H:i' ); ?> 
					<?php if ( tribe_get_end_date( $ev_id, false, 'H:i') !== '00:00' ) : ?>
						- <?php echo tribe_get_end_date( $ev_id, false, 'H:i' ); ?>
					<?php endif; ?>
				</span>

			<?php endif; ?>
		<?php else : ?>
			All Day Event
		<?php endif; ?>
	</td>

	<td class="location">
		<?php

		/*
		$ev_id = tribe_get_venue_id($ev_id); ?>
		<?php echo tribe_get_venue($venID);
		*/
		echo tribe_get_venue( $ev_id );
		?>
	</td>

	<td>
		<a href="/event/<?php echo get_post( $ev_id )->post_name; ?>/?ical=1">iCal</a>
		<?php /*<a href="webcal://bedfordschool.org.uk/event/<?php echo get_post($ev_id)->post_name; ?>/?ical=1">iCal</a>*/ ?>
	</td>
</tr>
