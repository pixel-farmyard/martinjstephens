<?php
/**
 * Template part to display a notification popup
 *
 * @package twkmedia
 */

// Cookies.
$cookie_name = get_field( 'cookies_bar_cookies_name', 'option' );
if ( '' === $cookie_name ) {
	$cookie_name = 'twk_cookies_bar_cookie_00001';
}
?>

<?php if ( get_field( 'cookies_bar_active', 'option' ) ) : ?>
	<div class="notification cookies-bar" style="display: none;">
		<div class="notification__content">
			<?php if ( get_field( 'cookies_bar_text', 'option' ) ) : ?>
				<div class="notification__text"><?php the_field( 'cookies_bar_text', 'option' ); ?></div>
			<?php endif; ?>

			<div class="buttons">
				<a href="#" class="button mx-2 button-accept">Accept</a>

				<?php
				if ( get_field( 'cookies_bar_link', 'option' ) ) :
					echo twk_compose_acf_link( get_field( 'cookies_bar_link', 'option' ), 'button mx-2 button-settings' );
				endif;
				?>
			</div>
		</div>
	</div>
	<?php
endif;
?>



<script>
	function defer(method) {
		if (window.jQuery) {
			method();
		} else {
			setTimeout(function() { defer(method) }, 50);
		}
	}
	defer(function () {
		(function ($){

			$(window).on('load', function() {
				function getCookie(name) {
					var dc = document.cookie;
					var prefix = name + "=";
					var begin = dc.indexOf("; " + prefix);
					if (begin == -1) {
						begin = dc.indexOf(prefix);
						if (begin != 0) return null;
					}
					else
					{
						begin += 2;
						var end = document.cookie.indexOf(";", begin);
						if (end == -1) {
						end = dc.length;
						}
					}
					// because unescape has been deprecated, replaced with decodeURI
					//return unescape(dc.substring(begin + prefix.length, end));
					return decodeURI(dc.substring(begin + prefix.length, end));
				}


				let cookie_name                       = "<?php echo esc_html( $cookie_name ); ?>";
				let cookies_notification_cookies_name = getCookie(cookie_name);

				if ( ! cookies_notification_cookies_name ){
					$( '.notification.cookies-bar' ).fadeIn();
				}

				// Accepts all cookies.
				$('.cookies-bar .button-accept').on('click', function(event){
					event.preventDefault();

					setCookie( cookie_name, 'true', 50000);
					setCookie( 'twk_cookies_google_analytics', 'true', 50000);  // GA/GTM.
					setCookie( 'twk_cookies_marketing', 'true', 50000);         // Notifications Popup.

					// Gtag Manager - Fire event.
					if (typeof dataLayer !== 'undefined') {
						dataLayer.push({'event': 'twk_cookies_consent_given'});
						dataLayer.push({'cookie_consent': 'true'});
					}

					$( ".notification.cookies-bar" ).fadeTo("slow", 0, function(){
						$(".notification.cookies-bar").css('pointer-events', 'none');
					});

				});

				function setCookie(cname, cvalue, exdays) {
					var d = new Date();
					d.setTime(d.getTime() + (exdays*24*60*60*1000));
					var expires = "expires="+ d.toUTCString();

					if ( exdays == 0 ){
						document.cookie = cname + "=" + cvalue + ";path=/";
					} else {
						document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
					}
				}

			});

		})(jQuery);
	});
</script>
