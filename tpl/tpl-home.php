<?php
/**
 * Template Name: Home
 */

get_header(); ?>
<div id="mjs-hero">
    
<?php 
    $image = get_field('header_image');
    if( $image ):

    // Image variables.
    $url = $image['url'];

    // Thumbnail size attributes.
    $size = 'large';
    $thumb = $image['sizes'][ $size ];
endif;
?>    
    
<div class="mjs-hero-img" style="background-image:url('<?php echo esc_url($thumb); ?>'"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-lg-11 col-xl-10 mjs-titles">
                <h1 class="title title--xl highlighter home-highlighter"><?php echo get_bloginfo( 'name' ); ?></h1>
                <div class="taglines">
                    <h2><?php echo get_field('tagline_1')?></h2>
                    <h2><?php echo get_field('tagline_2')?></h2>
                    <h2><?php echo get_field('tagline_3')?></h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="mjs-hero-intro col-lg-6 col-md-8 col-sm-12">
                <?php echo get_field('intro')?> 
            </div>
        </div>
    </div>
</div>

<div id="mjs-blog">
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mjs-blog-title">
                <h2 class="title--xl reverse-highlighter">Blog</h2> 
            </div>
        </div>
        <div class="row">
                
           <?php 
            // the query
                $the_query = new WP_Query( array(
                    'posts_per_page' => 6,
                )); 
            ?>

            <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-lg-6 col-md-12 blog-box">
                    <a class="" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><h4 class="highlighter"><?php the_title(); ?></h4></a>
                <p class="excerpt"><?php echo get_the_excerpt(); ?></p>
                <span class="blog-date blog-meta"><?php echo get_the_date( 'd M Y', $post->ID ) ?></span>&emsp;&bull;&emsp;
                <span class="blog-link blog-meta"><a class="link-highlighter" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">read this</a></span>
            </div>

            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>

            <?php endif; ?>
            
            
        </div>
        <div class="container-fluid">
        <div class="mjs-blog-more-container row justify-content-center">
                <a class="big-link-highlighter mjs-blog-more" href="<?php echo get_site_url()?>/blog/" title="more blogs">more blogs</a> 
            </div>
    </div>
</div>
</div>   



<div id="mjs-video">
    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-md-12 mjs-video-title">
                <h2 class="title--xl">Video</h2> 
            </div>
        </div>
            <div class="row justify-content-center ">
                <?php $loop = new WP_Query( array( 'post_type' => 'video', 'posts_per_page' => 3) ); ?>
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <div class="video-thumb col-xl-3 col-lg-7 col-md-9 col-sm-9" style="background:url(<?php the_post_thumbnail_url('medium'); ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                        <a class="magnific-video" href="<?php echo get_field( "video_link" ); ?>"><img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" /></a>
                        <a class="magnific-video video-link" href="<?php echo get_field( "video_link" ); ?>"></a>
                        <h3><a class="magnific-video" href="<?php echo get_field( "video_link" ); ?>"><?php echo get_the_title(); ?></a></h3>    
                </div>               

<?php endwhile; wp_reset_query(); ?>
                </div>
        <div class="row justify-content-md-center">
            <div class="col-lg-4 col-md-6 col-sm-8 mjs-video-more">
                <a class="big-link-highlighter" href="<?php echo get_site_url()?>/video/" title="more videos">more videos</a> 
            </div>
        </div>
    </div>
</div>

<div id="mjs-teasers">
     <div class="container-fluid">
         <div class="row justify-content-end">
             <h2 class="title--xl reverse-highlighter"><?php echo get_field('teasers_title');?></h2> 
         </div>
         <div class="row home-teasers">
            <a class="teaser large-teaser" href="<?php echo get_field('teaser_link_1') ?>">
            <div class="teaser-inner" style="background:url(<?php echo get_field('teaser_image_1') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                <h3><?php echo get_field('teaser_title_1') ?></h3>
            </div>
            </a>
            <a class="teaser small-teaser" href="<?php echo get_field('teaser_link_2') ?>">
            <div class="teaser-inner" style="background:url(<?php echo get_field('teaser_image_2') ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                <h3><?php echo get_field('teaser_title_2') ?></h3>
           </div>
            </a>
        </div>
    </div>

</div>

<?php get_footer();
