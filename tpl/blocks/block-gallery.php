<?php
/**
 * Template part to display the block called "Gallery".
 *
 * @package twkmedia
 */

$display       = get_sub_field( 'display' );
$open_in_popup = get_sub_field( 'open_in_popup' );
$gallery       = get_sub_field( 'gallery' );
?>

<?php if ( get_sub_field( 'title' ) || get_sub_field( 'intro' ) ) : ?>
	<div class="container mb-30">
		<div class="row justify-content-center">
			<div class="col-md-10 col-lg-8 text-center">
				<?php if ( get_sub_field( 'title' ) ) : ?>
					<h2 class="title title-lg">
						<?php echo get_sub_field( 'title' ); ?>
					</h2>
				<?php endif; ?>

				<?php if ( get_sub_field( 'intro' ) ) : ?>
					<p class="text"><?php echo get_sub_field( 'intro' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php
if ( $display === 'slider' ) :
	require locate_template( 'tpl/blocks/block-gallery--slider.php' );
elseif ( $display === 'gallery-slider' ) :
	require locate_template( 'tpl/blocks/block-gallery--gallery-slider.php' );
else :
	require locate_template( 'tpl/blocks/block-gallery--columns.php' );
endif;
