<?php
/**
 * Template part to display the block called "Blockquote".
 *
 * @package twkmedia
 */

if ( get_sub_field( 'quote' ) ) :
	?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 col-lg-8">
				<blockquote>
					<p><?php echo get_sub_field( 'quote' ); ?></p>

					<?php if ( get_sub_field( 'citation' ) ) : ?>
						<cite><?php echo get_sub_field( 'citation' ); ?></cite>
					<?php endif; ?>
				</blockquote>
			</div>
		</div>
	</div>
	<?php
endif;
