<?php
/**
 * Template part to display the partial for Block Gallery - Slider.
 *
 * @package twkmedia
 */

if ( $gallery ) :
	?>
	<div class="block--gallery--slider gallery">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="full-width-slider">
						<?php foreach ( $gallery as $image ) : ?>
							<div class="gallery__item object-fit object-fit--cover position-relative">

								<img src="<?php echo esc_url( $image['sizes']['large'] ); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>" />

							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
endif;
