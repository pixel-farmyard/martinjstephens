<?php
/**
 * Template part to display the block called "Columns text".
 *
 * @package twkmedia
 */

$number_columns = get_sub_field( 'number_of_columns' );
$bs_columns     = get_sub_field( 'bs_columns' );         // Only for 1 WYSIWYG editor.
$bs_row         = get_sub_field( 'bs_row' );
?>

<?php if ( get_sub_field( 'title' ) || get_sub_field( 'intro' ) ) : ?>
	<div class="container mb-30">
		<div class="row justify-content-center">
			<div class="col-md-8 text-center">
				<?php if ( get_sub_field( 'title' ) ) : ?>
					<h2 class="title title-lg">
						<?php echo get_sub_field( 'title' ); ?>
					</h2>
				<?php endif; ?>

				<?php if ( get_sub_field( 'intro' ) ) : ?>
					<p class="text"><?php echo get_sub_field( 'intro' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>


<?php if ( $number_columns === '1' ) : ?>

	<div class="container">
		<div class="row <?php echo $bs_row; ?>">
			<div class="col-md-<?php echo $bs_columns; ?>">
				<?php echo get_sub_field( 'editor_1' ); ?>
			</div>
		</div>
	</div>

<?php elseif ( $number_columns === '2' ) : ?>

	<div class="container">
		<div class="row <?php echo $bs_row; ?>">
			<div class="col-md-6">
				<?php echo get_sub_field( 'editor_1' ); ?>
			</div>
			<div class="col-md-6">
				<?php echo get_sub_field( 'editor_2' ); ?>
			</div>
		</div>
	</div>

<?php elseif ( $number_columns === '3' ) : ?>

	<div class="container">
		<div class="row <?php echo $bs_row; ?>">
			<div class="col-md-4">
				<?php echo get_sub_field( 'editor_1' ); ?>
			</div>
			<div class="col-md-4">
				<?php echo get_sub_field( 'editor_2' ); ?>
			</div>
			<div class="col-md-4">
				<?php echo get_sub_field( 'editor_3' ); ?>
			</div>
		</div>
	</div>

	<?php
endif;
