<?php
/**
 * Template part to display the partial for Block Gallery - Gallery Slider.
 *
 * @package twkmedia
 */

if ( $gallery ) :
	?>

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
				<div class="gallery-slider">
					<div class="gallery-slider__full-nav">
						<div class="pages"></div>
					</div>

					<?php $images = get_sub_field( 'gallery' ); ?>

					<div class="gallery-slider__full">
						<?php
						foreach ( $images as $image ) {
							$classes = '';

							echo '<img src="' . $image['sizes']['large'] . '" class="' . $classes . '">';
						}
						?>
					</div>
					<div class="gallery-slider__thumb">
						<?php
						foreach ( $images as $image ) {
							$classes = '';

							echo '<img src="' . $image['sizes']['thumbnail'] . '" class="' . $classes . '">';
						}
						?>
					</div>

				</div>
			</div>
		</div>
	</div>

	<?php
endif;
