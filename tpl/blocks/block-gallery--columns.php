<?php
/**
 * Template part to display the partial for Block Gallery in Columns.
 *
 * @package twkmedia
 */

if ( $gallery ) :
	?>
	<div class="block--gallery--columns gallery">
		<div class="container">	
			<div class="row">
				<?php foreach ( $gallery as $image ) : ?>
					<div class="col-md-<?php echo $display; ?> mb-30">
						<div class="gallery__item object-fit object-fit--cover position-relative">

							<img src="<?php echo esc_url( $image['sizes']['large'] ); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>" />

							<?php if ( $open_in_popup ) : ?>
								<a href="<?php echo esc_url( $image['url'] ); ?>" class="magnific-gallery-image stretched-link">
									<span class="sr-only"><?php echo esc_attr( $image['alt'] ); ?> Image</span>
								</a>
							<?php endif; ?>

						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<?php
endif;
