<?php
/**
 * Template part to display the block called "Accordion".
 *
 * @package twkmedia
 */

?>

<?php if ( get_sub_field( 'title' ) || get_sub_field( 'intro' ) ) : ?>
	<div class="container mb-30">
		<div class="row justify-content-center">
			<div class="col-md-10 col-lg-8 text-center">
				<?php if ( get_sub_field( 'title' ) ) : ?>
					<h2 class="title title-lg">
						<?php echo get_sub_field( 'title' ); ?>
					</h2>
				<?php endif; ?>

				<?php if ( get_sub_field( 'intro' ) ) : ?>
					<p class="text"><?php echo get_sub_field( 'intro' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>


<div class="accordion-container">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 col-lg-8">

				<!-- --------------------------------
					ACCORDION
				-------------------------------- -->
				<?php if ( have_rows( 'accordion' ) ) : ?>
					<div class="accordion">
						<?php
						while ( have_rows( 'accordion' ) ) :
							the_row();
							?>

							<div class="accordion__item">
								<div class="accordion__trigger js-accordion-trigger">
									<h3 class="accordion__title title title--md m-0">
										<?php the_sub_field( 'title' ); ?>
									</h3>

									<div class="accordion__trigger-icon js-accordion-trigger-icon">
										<!-- ARROW -->
										<!-- <svg width="18px" height="10px" viewBox="0 0 18 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
											<g id="Home-v3" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-440.000000, -785.000000)">
												<g id="Page-1" transform="translate(440.000000, 785.000000)" fill="#13212e">
													<path d="M5.95082782,12.8924648 L13.6658265,5.39228986 C13.8252119,5.23681359 13.9163654,5.02277461 13.9163654,4.80303482 C13.9163654,4.58381329 13.8252119,4.36873779 13.6658265,4.21377979 L5.95082782,-3.28639512 C5.61606522,-3.61134051 5.07340864,-3.61134051 4.7391791,-3.28639512 C4.40441649,-2.96093148 4.40441649,-2.4333487 4.7391791,-2.10788505 L11.8475538,4.80303482 L4.7391791,11.7139547 C4.40441649,12.0394183 4.40441649,12.5670011 4.7391791,12.8924648 C5.07340864,13.2179284 5.61606522,13.2179284 5.95082782,12.8924648" id="Fill-1" transform="translate(9.202236, 4.803229) rotate(90.000000) translate(-9.202236, -4.803229) "></path>
												</g>
											</g>
										</svg> -->

										<!-- PLUS -->
										<div class="accordion__plus js-accordion-plus"></div>
									</div>
								</div>

								<div class="accordion__content--wrapper js-accordion-target">
									<div class="accordion__content"><?php the_sub_field( 'content' ); ?></div>
								</div>
							</div>

						<?php endwhile; ?>
					</div>
				<?php endif; ?>
				<!-- END ACCORDION -->

			</div>
		</div>
	</div>
</div>
