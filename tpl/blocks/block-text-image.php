<?php
/**
 * Template part to display the block called "Text & Image".
 *
 * @package twkmedia
 */

$initial_text_position = get_sub_field( 'initial_text_position' );
$alternate_position    = get_sub_field( 'alternate_position' );
?>

<?php if ( get_sub_field( 'title' ) || get_sub_field( 'intro' ) ) : ?>
	<div class="container mb-30">
		<div class="row justify-content-center">
			<div class="col-md-10 col-lg-8 text-center">
				<?php if ( get_sub_field( 'title' ) ) : ?>
					<h2 class="title title-lg">
						<?php echo get_sub_field( 'title' ); ?>
					</h2>
				<?php endif; ?>

				<?php if ( get_sub_field( 'intro' ) ) : ?>
					<p class="text"><?php echo get_sub_field( 'intro' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>


<?php if ( have_rows( 'content' ) ) : ?>
	<div class="block--text-image__content">
		<div class="container">
			<?php
			$row_number = 0;
			while ( have_rows( 'content' ) ) :
				the_row();
				$row_number++;

				if ( ! $alternate_position ) {
					$text_position  = ( $initial_text_position === 'order-1' ? 'order-md-1' : 'order-md-2' );
					$image_position = ( $initial_text_position === 'order-1' ? 'order-md-2' : 'order-md-1' );
				} else {
					if ( $initial_text_position === 'order-1' ) {
						$text_position  = ( $row_number % 2 === 0 ? 'order-md-2' : 'order-md-1' );
						$image_position = ( $row_number % 2 === 0 ? 'order-md-1' : 'order-md-2' );
					} else {
						$text_position  = ( $row_number % 2 === 0 ? 'order-md-1' : 'order-md-2' );
						$image_position = ( $row_number % 2 === 0 ? 'order-md-2' : 'order-md-1' );
					}
				}

				$image_size = intval( get_sub_field( 'image_size' ) );
				$text_size  = 12 - $image_size;
				?>


				<div class="row my-30">
					<div class="col-md-<?php echo $text_size; ?> <?php echo $text_position; ?>">
						<?php if ( get_sub_field( 'text' ) ) : ?>
							<div class="block--text-image__content__text">
								<?php echo get_sub_field( 'text' ); ?>
							</div>
						<?php endif; ?>
					</div>

					<div class="col-md-<?php echo $image_size; ?> <?php echo $image_position; ?>">
						<?php if ( get_sub_field( 'image' ) ) : ?>
							<div
								class="block--text-image__content__image h-100 img-background img-background--cover"
								style="background: url( <?php echo get_sub_field( 'image' )['sizes']['large']; ?> );"
								aria-label="<?php echo get_sub_field( 'image' )['alt']; ?>"
							>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<?php
			endwhile;
			?>
		</div>
	</div>
<?php endif; ?>
