<?php
/**
 * Frontend functions.
 *
 * @package twkmedia
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Custom pagination.
 *
 * @param string  $pages Pages.
 * @param integer $range Range.
 * @return void
 */
function twk_custom_pagination( $pages = '', $range = 2 ) {
	$showitems = ( $range * 2 ) + 1;

	global $paged;
	if ( empty( $paged ) ) {
		$paged = 1;
	}

	if ( '' === $pages ) {
		global $wp_query;

		$pages = $wp_query->max_num_pages;

		if ( ! $pages ) {
			$pages = 1;
		}
	}

	if ( 1 !== $pages ) {
		echo "<div class='pagination'>";
		if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) {
			echo "<a href='" . get_pagenum_link( 1 ) . "'>&laquo;</a>";
		}
		if ( $paged > 1 && $showitems < $pages ) {
			echo "<a href='" . get_pagenum_link( $paged - 1 ) . "'>&lsaquo;</a>";
		}

		for ( $i = 1; $i <= $pages; $i++ ) {
			if ( 1 !== $pages && ( ! ( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
				echo ( $paged === $i ) ? "<span class='current'>" . $i . "</span>" : "<a href='" . get_pagenum_link( $i ) . "' class='inactive' >" . $i . "</a>";
			}
		}

		if ( $paged < $pages && $showitems < $pages ) {
			echo "<a href='" . get_pagenum_link( $paged + 1 ) . "'>&rsaquo;</a>";
		}
		if ( $paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages ) {
			echo "<a href='" . get_pagenum_link( $pages ) . "'>&raquo;</a>";
		}
		echo "</div>\n";
	}
}


/**
 * Get Yoast primary category link.
 *
 * @return link
 */
function twk_get_primary_cat_link() {
	if ( class_exists( 'WPSEO_Primary_Term' ) ) {
		$wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$term               = get_term( $wpseo_primary_term );

		// Default to first category (not Yoast) if an error is returned.
		if ( ! is_wp_error( $term ) ) {
			$category_display = $term->name;
			$category_link    = get_category_link( $term->term_id );
			return '<a href="' . $category_link . '">' . $category_display . '</a>';
		}
	}
}


/**
 * It creates a way to have custom lengths excerpts - ex. echo twk_excerpt(32);
 * param $limit integer Number of words to show on the excerpt
 * return $excerpt The excerpt with the limited number of words
 *
 * @param integer $limit Limit of words to show on the excerpt.
 * @return String $excerpt
 */
function twk_excerpt( $limit ) {
	$excerpt = explode( ' ', get_the_excerpt(), $limit );

	if ( count( $excerpt ) >= $limit ) {
		array_pop( $excerpt );
		$excerpt = implode( ' ', $excerpt ) . '...';
	} else {
		$excerpt = implode( ' ', $excerpt );
	}

	$excerpt = preg_replace( '`[[^]]*]`', '', $excerpt );

	return $excerpt;
}



/**
 * Creates an anchor tag with the appropriate link and data.
 *
 * @param array  $the_link ACF link field.
 * @param String $classes Classes to be added to the link.
 * @return String HTML
 */
function twk_compose_acf_link( $the_link, $classes = null ) {

	// Get the data.
	$link_url   = ( $the_link['url'] ) ? $the_link['url'] : '';                // URL.
	$link_title = ( $the_link['title'] ) ? $the_link['title'] : '';            // Title.
	if ( '' === $link_title ) {
		$link_title = get_the_title( url_to_postid( $link_url ) );
	}
	$link_target = ( $the_link['target'] ) ? $the_link['target'] : '_self';    // Target.

	// Use the data to create the link (html anchor).
	if ( '_blank' === $link_target ) {
		$return = '<a href="' . esc_url( $link_url ) . '" target="' . esc_attr( $link_target ) . '" rel="noopener noreferrer external" class="' . $classes . '">';
	} else {
		$return = '<a href="' . esc_url( $link_url ) . '" target="' . esc_attr( $link_target ) . '" class="' . $classes . '">';
	}
	$return .= esc_html( $link_title );
	$return .= '</a>';

	return $return;

}





/**
 * Gets the image url with a fallback system.
 *
 * @param integer $post_id The post ID.
 * @param string  $size The size of the image that will be returned.
 * @param string  $fallback The fallback image on the options setting page.
 * @return String $image_url Image url.
 */
function twk_get_image_url_with_fallback( $post_id = null, $size = 'large', $fallback = 'pages' ) {

	// Get the data.
	if ( null === $post_id ) {
		$post_id = get_the_ID();
	}

	$fallback_option = $fallback . '_fallback_image';

	if ( get_field( 'banner_image', $post_id ) ){
		if ( 'full' === $size ) {
			$image_url = get_field( 'banner_image', $post_id )['url'];               // banner image as first option.
		} else {
			$image_url = get_field( 'banner_image', $post_id )['sizes'][ $size ];    // banner image as first option.
		}
	}

	if ( ! isset( $image_url ) && get_the_post_thumbnail_url( $post_id, $size ) ) :
		$image_url = get_the_post_thumbnail_url( $post_id, $size );               // thumbnail as second option.
	endif;

	if ( ! isset( $image_url ) ) :
		if ( 'full' === $size ) {
			$image_url = get_field( $fallback_option, 'option' )['url'];             // fallback on the options page as last option.
		} else {
			$image_url = get_field( $fallback_option, 'option' )['sizes'][ $size ];  // fallback on the options page as last option.
		}
	endif;

	return $image_url;
}