<?php
/**
 * Redirections
 *
 * @package twkmedia
 */

/**
 * Gets the page id from the parameters on the URL.
 *
 * @return integer The page id if any. null if not found.
 */
function twk_get_page_id_from_url() {
	if ( isset( $_SERVER['QUERY_STRING'] ) ) {
		$visited_url = wp_unslash( $_SERVER['QUERY_STRING'] );
	} else {
		$visited_url = null;
	}

	$page_id     = null;
	$page        = null;
	$query_array = array();
	parse_str( $visited_url, $query_array );

	if ( array_key_exists( 'page_id', $query_array ) ) {
		$page_id = $query_array['page_id'];
	} elseif ( array_key_exists( 'p', $query_array ) ){
		$page_id = $query_array['p'];
	}

	if ( $page_id ) {
		$page = get_post( $page_id ); // to check if the page exists.
	}

	if ( $page ) {
		return $page_id;
	} else {
		return null;
	}
}

/**
 * Check before the 404 page.
 * Go to the login form if the link is for a draft page or private page.
 *
 * @return void
 */
function twk_before_404() {
	if ( is_404() ) {
		$page_id = twk_get_page_id_from_url();

		if ( $page_id ) {
			// Draft page.
			wp_safe_redirect( wp_login_url() . '?page_id=' . $page_id );
			exit();
		} else {
			// Private page.
			$page_requested = get_page_by_path( $_SERVER['REQUEST_URI'] );

			if ( $page_requested &&  get_post_status( $page_requested->ID ) == 'private' ) {
				wp_safe_redirect( wp_login_url() . '?page_id=' . $page_requested->ID );
			}
		}
	}
}
add_action( 'template_redirect', 'twk_before_404' );

/**
 * After login checks if there is a page ID on the URL and redirects to it.
 *
 * @param string $url The redirect destination URL.
 * @param string $request The requested redirect destination URL passed as a parameter.
 * @param object $user WP_User object if login was successful, WP_Error object otherwise.
 * @return string
 */
function twk_after_login_redirect( $url, $request, $user ) {
	$page_id = twk_get_page_id_from_url();

	if ( $page_id ) {
		$url = get_permalink( $page_id );
	}

	return $url;
}
add_filter( 'login_redirect', 'twk_after_login_redirect', 10, 3 );
