<?php
/**
 * Plugin specifics
 *
 * @package twkmedia
 */

/*
	==================================================================
		GRAVITY FORMS
	==================================================================
*/
/**
 * Permissions/capabilities for Gravity Forms
 *
 * @return void
 */
function twk_add_gf_cap() {
	$editor = get_role( 'editor' );
	$editor->add_cap( 'gravityforms_edit_forms' );
	$editor->add_cap( 'gravityforms_delete_forms' );
	$editor->add_cap( 'gravityforms_create_form' );
	$editor->add_cap( 'gravityforms_view_entries' );
	$editor->add_cap( 'gravityforms_edit_entries' );
	$editor->add_cap( 'gravityforms_delete_entries' );
	$editor->add_cap( 'gravityforms_view_settings' );
	$editor->add_cap( 'gravityforms_edit_settings' );
	$editor->add_cap( 'gravityforms_export_entries' );
	$editor->add_cap( 'gravityforms_view_entry_notes' );
	$editor->add_cap( 'gravityforms_edit_entry_notes' );
}
add_action( 'admin_init', 'twk_add_gf_cap' );




/*
	==================================================================
		RELEVANSSI
	==================================================================
*/
/**
 * Relevanssi add content to custom excerpts.
 *
 * @param [type] $content Content.
 * @param [type] $post Post.
 * @param [type] $query Query.
 * @return $content
 */
function twk_custom_fields_to_excerpts( $content, $post, $query ) {

	$custom_fields            = get_post_custom_keys( $post->ID );
	$remove_underscore_fields = true;

	if ( is_array( $custom_fields ) ) {
		$custom_fields = array_unique( $custom_fields ); // no reason to index duplicates.

		foreach ( $custom_fields as $field ) {

			if ( $remove_underscore_fields ) {
				if ( '_' === substr( $field, 0, 1 ) ) {
					continue;
				}
			}

			$values = get_post_meta( $post->ID, $field, false );

			if ( '' === $values ) {
				continue;
			}
			foreach ( $values as $value ) {
				if ( ! is_array( $value ) ) {
					$content .= ' ' . $value;
				}
			}
		}
	}

	return $content;
}
add_filter( 'relevanssi_excerpt_content', 'twk_custom_fields_to_excerpts', 10, 3 );




/*
	==================================================================
		ADVANCED CUSTOM FIELDS (ACF)
	==================================================================
*/
/**
 * Add ACF Global Options page.
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
		array(
			'page_title' => 'Global Settings',
			'menu_title' => 'Global Settings',
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
	);
	acf_add_options_sub_page(
		array(
			'page_title'  => 'Images',
			'menu_title'  => 'Images',
			'parent_slug' => 'theme-general-settings',
		)
	);
	acf_add_options_sub_page(
		array(
			'page_title'  => 'Notifications',
			'menu_title'  => 'Notifications',
			'parent_slug' => 'theme-general-settings',
		)
	);
	acf_add_options_sub_page(
		array(
			'page_title'  => 'Footer Settings',
			'menu_title'  => 'Footer',
			'parent_slug' => 'theme-general-settings',
		)
	);
	acf_add_options_sub_page(
		array(
			'page_title'  => '404 Settings',
			'menu_title'  => '404',
			'parent_slug' => 'theme-general-settings',
		)
	);
}


/**
 * Add new location for ACF json data.
 *
 * @param [type] $path Path.
 * @return $path
 */
function twk_acf_json_save_point( $path ) {
	// update path.
	$path = get_stylesheet_directory() . '/lib/acf-json';

	// return.
	return $path;
}
add_filter( 'acf/settings/save_json', 'twk_acf_json_save_point' );

/**
 * Undocumented function
 *
 * @param [type] $paths Paths.
 * @return $paths
 */
function twk_acf_json_load_point( $paths ) {
	// remove original path (optional).
	unset( $paths[0] );

	// append path.
	$paths[] = get_stylesheet_directory() . '/lib/acf-json';

	// return.
	return $paths;
}
add_filter( 'acf/settings/load_json', 'twk_acf_json_load_point' );




/**
 * Set default flexible content strips. Starter template.
 *
 * @param [type] $value
 * @param [type] $post_id
 * @param [type] $field
 * @return void
 */
function twk_default_blocks( $value, $post_id, $field ) {

	// Only add default content for new posts.
	if ( $value !== null ) {
		return $value;
	}

	// Only add default content for certain post types.
	if ( ! in_array( get_post_type( $post_id ), array( 'page' ), true ) ) {
		return $value;
	}

	$value = array(
		array(
			'acf_fc_layout' => 'wysiwyg_editor',
		),
	);

	return $value;
}
add_filter( 'acf/load_value/name=blocks', 'twk_default_blocks', 10, 3 );





/**
 * Function that runs after a post is saved or udpated.
 *
 * @return void
 */
function twk_acf_save_post() {

	$screen = get_current_screen(); // We need to know what screen on the backend was saved.

	/**
	 * Cookies.
	 */
	if (
		strpos( $screen->id, 'acf-options-notifications' ) == true && get_field( 'cookies_bar_reset', 'option' ) ||
		get_field( 'cookies_bar_cookies_name', 'option' ) === ''
		) {
		// 'global-settings_page_acf-options-notifications' is the slug for the "option page" of ACF where we manage the notifications fields
		// 'cookies_bar_reset' is a true/false field that we set to true when we want to reset the cookies
		$cookie_name = 'twk_cookies_bar_cookie_' . wp_rand( 1, 99999999999999999 ); // generates a new cookie number.

		update_field( 'cookies_bar_cookies_name', $cookie_name, 'option' );
		// 'cookies_bar_cookies_name' is a hidden field that controls the name of the cookie (you can add a class of hidden to the field and this won't show up on the backend)
	}

	/**
	 * Popup.
	 */
	if (
		strpos( $screen->id, 'acf-options-notifications' ) == true && get_field( 'popup_reset', 'option' ) ||
		get_field( 'popup_cookies_name', 'option' ) === ''
		) {
		// 'global-settings_page_acf-options-notifications' is the slug for the "option page" of ACF where we manage the notifications fields
		// 'cookies_bar_reset' is a true/false field that we set to true when we want to reset the cookies
		$cookie_name = 'twk_cookies_popup_' . wp_rand( 1, 99999999999999999 ); // generates a new cookie number.

		update_field( 'popup_cookies_name', $cookie_name, 'option' );
		// 'popup_cookies_name' is a hidden field that controls the name of the cookie (you can add a class of hidden to the field and this won't show up on the backend)
	}
}
add_action( 'acf/save_post', 'twk_acf_save_post', 20 );




/**
 * Slugifies the custom ID if one provided.
 *
 * @param integer $post_id The Post ID.
 * @return void
 */
function twk_clean_custom_block_id( $post_id ) {

	$strips    = 'blocks';
	$custom_id = 'block_custom_id';

	$count = count( get_post_meta( $post_id, $strips, true ) );

	for ( $i = 0; $i <= $count; $i++ ) {
		$custom_id_acf_name = $strips . '_' . $i . '_' . $custom_id;
		$value_custom_id    = get_post_meta( $post_id, $custom_id_acf_name, true );

		if ( $value_custom_id !== '' ) {
			$new_value_custom_id = sanitize_title( $value_custom_id );

			update_post_meta( $post_id, $custom_id_acf_name, $new_value_custom_id );
		}
	}

}
add_action( 'acf/save_post', 'twk_clean_custom_block_id' );






/*
	==================================================================
		Yoast SEO
	==================================================================
*/
/**
 * Move Yoast to bottom
 *
 * @return String low priority
 */
function twk_yoast_to_bottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'twk_yoast_to_bottom' );





/*
	==================================================================
		WP Rocket
	==================================================================
*/

/**
 * Allow editors to clear WP Rocket cache.
 *
 * @return void
 */
function wp_rocket_add_purge_posts_to_author() {
	// gets the author role object.
	$role = get_role( 'editor' );

	// add a new capability.
	$role->add_cap( 'rocket_purge_cache', true );
	//$role->add_cap( 'rocket_purge_cloudflare_cache', true );
}
add_action( 'init', 'wp_rocket_add_purge_posts_to_author', 12 );


/**
 * Filter to disable the table of contents test
 */
add_filter( 'rank_math/researches/tests', function( $tests, $type ) {
    unset(
        $tests['contentHasTOC'],
    );
    return $tests;
}, 10, 2 );