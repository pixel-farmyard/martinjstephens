<?php
/**
 * Shortcodes available.
 *
 * @package twkmedia
 */

/**
 * Custom WordPress gallery code, remove dt, dl - add to ul li instead.
 * This means user won't be able to set number of items per row, but can be more customised and responsive.
 *
 * @param array  $attr An associative array of attributes, or an empty string if no attributes are given.
 * @param string $content The enclosed content (if the shortcode is used in its enclosing form).
 * @return shortcode
 */

// remove default shortcode.
remove_shortcode( 'gallery' );

// add new shortcode.
add_shortcode( 'gallery', 'custom_gallery' );

/**
 * Custom gallery shortcode.
 *
 * @param [type] $attr
 * @return void
 */
function custom_gallery( $attr ) {
	$post = get_post();

	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) ) {
			$attr['orderby'] = 'post__in';
		}
		$attr['include'] = $attr['ids'];
	}

	$output = '';

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement.
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( ! $attr['orderby'] ) {
			unset( $attr['orderby'] );
		}
	}

	extract(
		shortcode_atts(
			array(
				'order'      => 'ASC',
				'orderby'    => 'menu_order ID',
				'id'         => $post ? $post->ID : 0,
				'icontag'    => 'figure',
				'captiontag' => 'p',
				'columns'    => 3,
				'size'       => 'thumbnail',
				'include'    => '',
				'exclude'    => '',
				'link'       => '',
				'class'      => '', // now you can add custom class to container UL .
			),
			$attr,
			'gallery'
		)
	);

	$id = intval( $id );

	if ( 'RAND' == $order ) {
		$orderby = 'none';
	}

	if ( ! empty( $include ) ) {

		$_attachments = get_posts(
			array(
				'include'        => $include,
				'post_status'    => 'inherit',
				'post_type'      => 'attachment',
				'post_mime_type' => 'image',
				'order'          => $order,
				'orderby'        => $orderby,
			)
		);

		$attachments = array();

		foreach ( $_attachments as $key => $val ) {
			$attachments[ $val->ID ] = $_attachments[ $key ];
		}

	} elseif ( ! empty( $exclude ) ) {

		$attachments = get_children(
			array(
				'post_parent'    => $id,
				'exclude'        => $exclude,
				'post_status'    => 'inherit',
				'post_type'      => 'attachment',
				'post_mime_type' => 'image',
				'order'          => $order,
				'orderby'        => $orderby,
			)
		);

	} else {

		$attachments = get_children(
			array(
				'post_parent'    => $id,
				'post_status'    => 'inherit',
				'post_type'      => 'attachment',
				'post_mime_type' => 'image',
				'order'          => $order,
				'orderby'        => $orderby,
			)
		);

	}

	if ( empty( $attachments ) ) {
		return '';
	}

	// if ( is_feed() ) //removed, see original in media.php

	$itemtag    = tag_escape( 'li' );          // new tag for item .
	$captiontag = tag_escape( $captiontag );
	$icontag    = tag_escape( $icontag );

	// valid tags check removed, see original in media.php

	$columns  = intval( $columns );
	$selector = "gallery-{$instance}";

	$size_class = sanitize_html_class( $size );

	// new tag for container .
	$output = "<ul id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class} {$class}'>";

	$i = 0;
	$c = count( $attachments );
	$o = (int) floor( $c / $columns ) * $columns;

	foreach ( $attachments as $id => $attachment ) {

		$image_output_small = wp_get_attachment_image_src( $id, 'thumbnail', false, false )[0];
		$image_output_large = wp_get_attachment_image_src( $id, 'full', false, false )[0];
		$image_meta         = wp_get_attachment_metadata( $id );
		$orientation        = '';

		if ( isset( $image_meta['height'], $image_meta['width'] ) ) $orientation = ($image_meta['height'] > $image_meta['width']) ? 'portrait' : 'landscape';

		// setup custom aux classes to help style.
		$m              = ++$i % $columns;
		$item_pos_class = ($m == 1) ? 'first-in-row' : (($m == 0) ? 'last-in-row' : '');
		$row_class      = ($i <= $columns) ? 'first-row' : (($i > $o) ? 'last-row' : '');

		// added: custom aux classes.
		$output .= "<{$itemtag} class='gallery-item {$item_pos_class} {$row_class}'>";
		$output .= "<a href='$image_output_large' class='magnific-gallery-image' title='" . wptexturize( $attachment->post_excerpt ) . "'>";
		$output .= "<div class='gallery__image' style='background-image: url($image_output_small);'></div>";
		$output .= "</a>";

		if ( $captiontag && trim( $attachment->post_excerpt ) ) {

			$output .= "<{$captiontag} class='wp-caption-text gallery-caption'>" . wptexturize( $attachment->post_excerpt ) . "</{$captiontag}>";

		}

		$output .= "</{$itemtag}>";

	}

	// changed BR>clear:both with a more conventional clearfix div.
	$output .= "</ul>\n<div class='clearfix'></div>";

	$output = preg_replace( '/(width|height)=\"\d*\"\s/', "", $output );

	return $output;
}
// end custom gallery.

