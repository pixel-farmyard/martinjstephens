<?php 

/* 

Generate your access token here: https://www.thewebkitchen.co.uk/instagram-token-generator/

Use like the following:

<?php
                                                      
$token = get_field('instagram_access_token', 'option');
$amountOfPosts = 3;

if ($token) {

    $instagramData = get_instagram_data($token, $amountOfPosts);

    // print_r($instagramData); // debugging

    foreach($instagramData->data as $instagramPost) {

	    $media_type = $instagramPost->media_type;
	    
		echo '<div class="insta-acc"><a href="https://www.instagram.com/' . $instagramPost->username . '" rel="nofollow" target="_blank"><span>@' . $instagramPost->username . '</span></a></div>';
		
		if ($media_type == 'VIDEO') {

			echo '
			<a href="https://www.instagram.com/' . $instagramPost->username . '" rel="nofollow" target="_blank">
			<video autoplay loop class="video-wrap__video" muted plays-inline style="width: 100%; height: auto; margin-bottom: 10px;">
				<source src=' . $instagramPost->media_url . ' type="video/mp4">
			</video>
			</a>
			';												
			
		} else {	
		
        	echo '<a href="' . $instagramPost->media_url . '" class="magnific-gallery-single-image" title="' . $instagramPost->caption . '"><img src="' . $instagramPost->media_url . '"></a>';
        
        }
        
        echo '<p>' . $instagramPost->caption . '</p>';
        
        echo '<div style="display: none">';
        
        print_r($instagramPost);
        
        echo '</div>';

    };
}

?>

*/

// sets up the instagram related fields on the global settings page in the backend so that the developer doesn't have to.
function setUpInstagramAcfFieldGroup() {
	
	acf_add_local_field_group(array(
		'key' => 'group_' . uniqid(),
		'title' => 'Instagram Data',
		'fields' => array (
			array (
				'key' => 'field_instagram_access_token_1234567890',
				'label' => 'Access Token',
				'name' => 'instagram_access_token',
				'type' => 'text',
                'instructions' => 'Generate your Instagram token with TWK\'s <a href="https://www.thewebkitchen.co.uk/instagram-token-generator/" target="_blank">Instagram Token Generator</a> and enter it here for the developer to use to display your Instagram feed.<br />If your token expires, simply generate a new token and replace it here.',
                'wrapper' => array (
                    'width' => '50%',
                    'class' => '',
                    'id' => '',
                ),
			),
            array (
				'key' => 'field_instagram_token_last_refresh_1234567890',
				'label' => 'Last refresh',
				'name' => 'instagram_token_last_refresh',
				'type' => 'text',
                'instructions' => 'The content in this field is automatically generated and is used to determine if your Instagram token needs to be refreshed or not.<br />Tokens should be automatically refreshed, so you can ignore this.',
                'wrapper' => array (
                    'width' => '50%',
                    'class' => '',
                    'id' => '',
                ),
                'readonly' => true
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page', 
					'operator' => '==',
					'value' => 'theme-general-settings',
				),
			),
		),
	));
}

add_action('acf/init', 'setUpInstagramAcfFieldGroup');



use TWK\InstagramBasicDisplay\InstagramBasicDisplay;

function refreshToken($instagramClass, $token) {

    return $instagramClass->refreshToken($token, true);

}

function checkTokenRefresh($instagramClass, $token) {

    $dateTimeNow = date('Y-m-d H:i:s');

    if (get_field('instagram_token_last_refresh', 'option')) {

        $lastRefreshDate = get_field('instagram_token_last_refresh', 'option');

        // date in file is older than 30 days
        if (strtotime($lastRefreshDate) < strtotime('-30 days')) {

            $refreshedToken = refreshToken($instagramClass, $token);

            update_field('instagram_access_token', $refreshedToken, 'option');
            update_field('instagram_token_last_refresh', $dateTimeNow, 'option');
            
        } else {
            
            // do nothing - token is not older than 30 days
        }

    } else {
        
        update_field('instagram_token_last_refresh', $dateTimeNow, 'option');
    }
}

function get_instagram_data($accessToken, $count) {

    $dataFile = 'instagram-data.json';

    if (file_exists($dataFile) && filemtime($dataFile) > time() - 60 * 30 ) {

        $instagramData = file_get_contents($dataFile);
        $instagramData = json_decode($instagramData);

    } else {

        $instagram = new InstagramBasicDisplay($accessToken);
        
        // Get the users profile
        $instagramData = $instagram->getUserMedia('me', $count);

        // Update the cache file
        file_put_contents($dataFile, json_encode($instagramData));

        // Check if the access token needs to be refreshed
        checkTokenRefresh($instagram, $accessToken);
    }
    
    return $instagramData;
}

?>
