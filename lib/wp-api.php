<?php
/**
 * WP API
 *
 * @package twkmedia
 */

/**
 * Filter to disable WP API endpoints.
 */
add_filter(
	'rest_endpoints',
	function( $endpoints ) {
		if ( isset( $endpoints['/wp/v2/users'] ) ) {
			unset( $endpoints['/wp/v2/users'] );
		}
		if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
			unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
		}
		if ( isset( $endpoints['/wp/v2'] ) ) {
			unset( $endpoints['/wp/v2'] );
		}
		if ( isset( $endpoints['/wp/v2/(?P<id>[\d]+)'] ) ) {
			unset( $endpoints['/wp/v2/(?P<id>[\d]+)'] );
		}

		return $endpoints;
	}
);
