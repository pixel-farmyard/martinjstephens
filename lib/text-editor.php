<?php
/**
 * Text editor functions
 *
 * @package twkmedia
 */

if ( ! function_exists( 'twkmedia_theme_setup' ) ) {
	/**
	 * ADD TinyMCE Button for Button styling
	 *
	 * @return void
	 */
	function twkmedia_theme_setup() {
		// Registers an editor stylesheet for the theme.
		add_action( 'admin_init', 'twkmedia_theme_add_editor_styles' );
	}
}
add_action( 'after_setup_theme', 'twkmedia_theme_setup' );


if ( ! function_exists( 'twkmedia_theme_add_editor_styles' ) ) {
	/**
	 * Registers an editor stylesheet for the theme.
	 *
	 * @return void
	 */
	function twkmedia_theme_add_editor_styles() {
		add_editor_style( 'assets/css/editor-style.css' );
	}
}



/**
 * Customize TinyMCE toolbars - Default TinyMCE editor.
 *
 * @param array $arr Array of block formats allow.
 * @return $arr      Array of block formats allow modified.
 */
function twk_tinymce_toolbars( $arr ) {
	$arr['toolbar1'] = 'styleselect, bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,link,unlink,undo,redo,spellchecker,dfw';

	$arr['toolbar2'] = 'outdent,indent,pastetext,removeformat,charmap,wp_more,table,wp_help';

	$arr['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4';

	return $arr;
}
add_filter( 'tiny_mce_before_init', 'twk_tinymce_toolbars' );




/**
 * Customize TinyMCE toolbars - ACF pro TinyMCE editor.
 *
 * @param array $toolbars TinyMCE toolbars.
 * @return array $toolbars The toolbars updated.
 */
function twk_custom_toolbars( $toolbars ) {

	// Toolbar 1.
	$toolbar_1_remove = array( 'formatselect' );
	foreach ( $toolbar_1_remove as $remove_item ) {
		if ( ( $key = array_search( $remove_item, $toolbars['Full'][1], true ) ) !== false ) {
			unset( $toolbars['Full'][1][ $key ] );
		}
	}

	// Add the "Format dropdown" to the first row.
	array_unshift( $toolbars['Full'][1], 'styleselect' );

	// Toolbar 2.
	$toolbar_2_remove = array( 'fontselect', 'fontsizeselect', 'forecolor' );
	foreach ( $toolbar_2_remove as $remove_item ) {
		if ( ( $key = array_search( $remove_item, $toolbars['Full'][2], true ) ) !== false ) {
			unset( $toolbars['Full'][2][ $key ] );
		}
	}

	return $toolbars;
}
add_filter( 'acf/fields/wysiwyg/toolbars', 'twk_custom_toolbars' );





/**
 * Callback function to filter the MCE settings
 *
 * @param [type] $init_array Array.
 * @return $init_array
 */
function twk_mce_before_init_insert_formats( $init_array ) {
	// Define the style_formats array.
	$style_formats = array(
		// Each array child is a format with it's own settings.
		array(
			'title'   => 'Paragraph',
			'block'   => 'p',
			'wrapper' => false,
		),
		array(
			'title'   => 'Heading 2',
			'block'   => 'h2',
			'wrapper' => false,
		),
		array(
			'title'   => 'Heading 3',
			'block'   => 'h3',
			'wrapper' => false,
		),
		array(
			'title'   => 'Heading 4',
			'block'   => 'h4',
			'wrapper' => false,
		),
		array(
			'title'    => 'Button',
			'selector' => 'a',
			'classes'  => 'button',
		),
	);

	// Insert the array, JSON ENCODED, into 'style_formats'.
	$init_array['style_formats'] = wp_json_encode( $style_formats );

	return $init_array;

}
add_filter( 'tiny_mce_before_init', 'twk_mce_before_init_insert_formats' );




/**
 * Hide editor on pages.

function hide_editor() {

	remove_post_type_support( 'page', 'editor' );

}
add_action( 'admin_init', 'hide_editor' );

 */
