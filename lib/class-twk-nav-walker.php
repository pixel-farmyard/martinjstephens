<?php
/**
 * Custom Nav Walker
 *
 * @package twkmedia
 */

/**
 * Fetch titles
 *
 * @param String   $item_output The menu items starting HTML output.
 * @param WP_Post  $item Menu item data object.
 * @param int      $depth Depth of menu item. Used for padding.
 * @param stdClass $args An object of wp_nav_menu() arguments.
 * @return $item_output
 */
function add_menu_description( $item_output, $item, $depth, $args ) {
	global $title_parent;
	$title_parent = $item->title;
	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'add_menu_description', 10, 4 );

/**
 * Setup Class.
 */
class Twk_Nav_Walker extends Walker_Nav_Menu {

	/**
	 * Starts the list before the elements are added.
	 *
	 * @param String  $output Used to append additional content (passed by reference).
	 * @param integer $depth Depth of menu item. Used for padding.
	 * @param array   $args (stdClass) An object of wp_nav_menu() arguments.
	 * @return void
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		global $title_parent;

		$indent  = str_repeat( '\t', $depth );
		$output .= "\n$indent<div class='sub-menu-wrap'><ul class='sub-menu'><a href='" . get_permalink( get_page_by_title( $title_parent ) ) . "'><span class='page-title'>" . $title_parent . "</span></a>\n";
	}

	/**
	 * Ends the list of after the elements are added.
	 *
	 * @param String  $output Used to append additional content (passed by reference).
	 * @param integer $depth Depth of menu item. Used for padding.
	 * @param array   $args (stdClass) An object of wp_nav_menu() arguments.
	 * @return void
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent  = str_repeat( '\t', $depth );
		$output .= "<div class='back-menu'>BACK</div>$indent</ul></div>\n";
	}

}
