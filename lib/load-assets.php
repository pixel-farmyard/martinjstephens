<?php
/**
 * Load assets
 *
 * @package twkmedia
 */

/**
 * Load CSS and Javascript files
 */
function load_theme_assets() {
	// Register and load CSS.
	wp_enqueue_style( 'style-css', get_stylesheet_uri() );

	// Register and load JS.
	wp_register_script( 'modernizr-js', get_template_directory_uri() . '/assets/js/vendor/modernizr-3.6.0.min.js', false, null, false );
	wp_enqueue_script( 'modernizr-js' );

//	wp_register_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/vendor/bootstrap.js', false, null, true );
//	wp_enqueue_script( 'bootstrap-js' );

	wp_register_script( 'magnific-js', get_template_directory_uri() . '/assets/js/vendor/jquery.magnific-popup.min.js', false, null, true );
	wp_enqueue_script( 'magnific-js' );

	wp_register_script( 'slick-js', get_template_directory_uri() . '/assets/js/vendor/slick.min.js', false, null, true );
	wp_enqueue_script( 'slick-js' );

	wp_register_script( 'script-js', get_template_directory_uri() . '/assets/js/script.js', array( 'jquery' ), null, true );

	// EXAMPLE OF HOW TO USE PHP VARIABLES IN JS FILE> CAN BE USEFUL FOR GETTING home_url().
	wp_localize_script( 'script-js', 'php_vars', array( 'themeDirUrl' => get_template_directory_uri(), 'homeUrl' => home_url() ) );
	wp_enqueue_script( 'script-js' );

}
add_action( 'wp_enqueue_scripts', 'load_theme_assets' );
