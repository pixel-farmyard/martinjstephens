<?php
/**
 * All custom post and taxonomy registerations.
 *
 * @package twkmedia
 */

// Custom post types goes here.

function mjs_custom_post_type() {
     register_post_type('video',
        array(
            'labels'      => array(
                'name'          => __('Video', 'textdomain'),
                'singular_name' => __('Video', 'textdomain'),
            ),
               
            'public'      => true,
            'has_archive' => true,
            'menu_icon' => 'dashicons-format-video',
            
            'rewrite' => array(
			     'slug' => 'video',
			     'with_front' => false
		),
                
            'supports' => array('title','thumbnail'),
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'capability_type'    => 'post',
                'hierarchical'       => false,
                'menu_position'      => null,
        )
    );

}

add_action('init', 'mjs_custom_post_type');
