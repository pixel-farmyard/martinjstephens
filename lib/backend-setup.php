<?php
/**
 * Backend Setup
 *
 * @package twkmedia
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

// get the the role object.
$role_object = get_role( 'editor' );
// add $cap capability to this role object.
$role_object->add_cap( 'edit_theme_options' );


// post thumbnail support.
add_theme_support( 'post-thumbnails' );

// Update default thumbnails sizes.
update_option( 'thumbnail_size_w', 400 );
update_option( 'thumbnail_size_h', 0 );
update_option( 'thumbnail_crop', '0');

update_option( 'medium_size_w', 1280 );
update_option( 'medium_size_h', 0 );

update_option( 'large_size_w', 1920 );
update_option( 'large_size_h', 0 );

remove_image_size( '1536x1536' );
remove_image_size( '2048x2048' );

//add_image_size( 'avatar', 150, 150, 1 );



// add html5 search form.
add_theme_support( 'html5', array( 'search-form' ) );



// custom menu support.
add_theme_support( 'menus' );

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
			'main_menu'   => 'Main Menu',
			'footer_menu' => 'Footer Menu',
            'socials_menu' => 'Socials Menu',
		)
	);
}



/**
 * Load CSS file for the admin area.
 *
 * @return void
 */
function load_custom_wp_admin_style() {

	wp_register_style( 'admin-style', get_stylesheet_directory_uri() . '/assets/css/admin-style.css', null, false, 'all' );
	wp_enqueue_style( 'admin-style' );

}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );



/**
 * Custom styles on the login page.
 *
 * @return void
 */
function twk_custom_login() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_stylesheet_directory_uri() . '/assets/css/custom-style-login.css" />';
}
add_action( 'login_head', 'twk_custom_login' );

/**
 * Custom logo login url
 *
 * @return url New url to link the logo.
 */
function twk_logo_login_url_removal() {
	return 'https://www.thewebkitchen.co.uk';
}
add_filter( 'login_headerurl', 'twk_logo_login_url_removal' );


/**
 * Login page custom title
 *
 * @return String Custom title.
 */
function twk_login_custom_title() {
	return 'THE WEB KITCHEN';
}
add_filter( 'login_headertext', 'twk_login_custom_title' );



// add support for post formats
// add_theme_support( 'post-formats', array('aside', 'link', 'quote')); 


/**
 * Changing excerpt length
 *
 * @param integer $length Lenght of the excerpt.
 * @return integer $length New excerpt length.
 */
function twk_new_excerpt_length( $length ) {
	return 22;
}
add_filter( 'excerpt_length', 'twk_new_excerpt_length', 999 );


/**
 * Changing excerpt more
 *
 * @param String $more The string shown within the more link.
 * @return String
 */
function twk_new_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'twk_new_excerpt_more' );




/**
 * Change default link in gallery settings to Media file instead of Attachment page
 *
 * @param array $settings List of media view settings.
 * @return array $settings
 */
function twk_my_gallery_default_type_set_link( $settings ) {
	$settings['galleryDefaults']['link'] = 'file';
	return $settings;
}
add_filter( 'media_view_settings', 'twk_my_gallery_default_type_set_link' );

/**
 * Change default size in gallery settings to Medium instead of Thumbnail
 *
 * @param array $settings List of media view settings.
 * @return array $settings
 */
function twk_my_gallery_default_type_set_size( $settings ) {
	$settings['galleryDefaults']['size'] = 'medium';
	return $settings;
}
add_filter( 'media_view_settings', 'twk_my_gallery_default_type_set_size' );





/**
 * Remove 'Protected' and 'Private' from titles of pages
 *
 * @param String $text Title.
 * @return String $text
 */
function twk_protect_my_privates( $text ) {
	$text = '%s';

	return $text;
}
//add_filter( 'private_title_format', 'twk_protect_my_privates' );
//add_filter( 'protected_title_format', 'twk_protect_my_privates' );





/**
	Remove the unneccessary junk WordPress adds to the header, by ThemeLab.
	http://www.themelab.com/remove-code-wordpress-header/
*/
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'start_post_rel_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'adjacent_posts_rel_link' );



/**
 * Updates the count for comments and trackbacks
 * Remove Trackbacks From Comments v1.1, by Weblog Tools Collection.
 * http://weblogtoolscollection.com/archives/2008/03/08/managing-trackbacks-and-pingbacks-in-your-wordpress-theme/
 *
 * @param array $comms Array of comments supplied to the comments template.
 * @return array $comments
 */
function twk_filter_trackbacks( $comms ) {
	global $comments, $trackbacks;

	$comments = array_filter( $comms, 'twk_strip_trackback' );

	return $comments;
}
add_filter( 'comments_array', 'twk_filter_trackbacks', 0 );


/**
 * Strips out trackbacks/pingbacks
 *
 * @param array $var Comments.
 * @return boolean
 */
function twk_strip_trackback( $var ) {
	if ( 'trackback' === $var->comment_type || 'pingback' === $var->comment_type ) {
		return false;
	}

	return true;
}


/**
 * Updates the comment number for posts with trackbacks
 *
 * @param array $posts List of posts.
 * @return array $posts
 */
function twk_filter_post_comments( $posts ) {
	foreach ( $posts as $key => $p ) {

		if ( $p->comment_count <= 0 ) {
			return $posts;
		}

		$comments                     = get_approved_comments( (int) $p->ID );
		$comments                     = array_filter( $comments, 'twk_strip_trackback' );
		$posts[ $key ]->comment_count = count( $comments );

	}
	return $posts;
}
add_filter( 'the_posts', 'twk_filter_post_comments', 0 );




/**
 * No Self Pings v1.0, by WPMU.
 * http://wpmu.org/daily-tip-3-ways-to-remove-wordpress-self-pings/
 *
 * @param array $links Links.
 * @return void
 */
function twk_no_self_ping( &$links ) {
	$home = get_option( 'home' );

	foreach ( $links as $l => $link ) {
		if ( 0 === strpos( $link, $home ) ) {
			unset( $links[ $l ] );
		}
	}
}
add_action( 'pre_ping', 'twk_no_self_ping' );





/**
 * Remove Comments without plugin.
 * http://themelovin.com/remove-wordpress-comments-without-plugins/
 * Close comments on the front-end.
 *
 * @return boolean
 */
function twk_disable_comments_status() {
	return false;
}
add_filter( 'comments_open', 'twk_disable_comments_status', 20, 2 );
add_filter( 'pings_open', 'twk_disable_comments_status', 20, 2 );

/**
 * Disable support for comments and trackbacks in post types.
 *
 * @return void
 */
function twk_disable_comments_post_types_support() {
	$post_types = get_post_types();

	foreach ( $post_types as $post_type ) {
		if ( post_type_supports( $post_type, 'comments' ) ) {
			remove_post_type_support( $post_type, 'comments' );
			remove_post_type_support( $post_type, 'trackbacks' );
		}
	}
}
add_action( 'admin_init', 'twk_disable_comments_post_types_support' );

/**
 * Remove comments page in menu
 *
 * @return void
 */
function twk_disable_comments_admin_menu() {
	remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'twk_disable_comments_admin_menu' );

/**
 * Remove comments links from admin bar
 *
 * @return void
 */
function twk_disable_comments_admin_bar() {
	if ( is_admin_bar_showing() ) {
		remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );
	}
}
add_action( 'admin_bar_menu', 'twk_disable_comments_admin_bar' );

/**
 * Remove comments metabox from dashboard
 *
 * @return void
 */
function twk_disable_comments_dashboard() {
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
}
add_action( 'admin_init', 'twk_disable_comments_dashboard' );


/**
 * Redirect any user trying to access comments page
 *
 * @return void
 */
function twk_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ( 'edit-comments.php' === $pagenow ) {
		wp_safe_redirect( admin_url() );
		exit;
	}
}
add_action( 'admin_init', 'twk_disable_comments_admin_menu_redirect' );




// Remove welcome panel from WordPress dashboard.
remove_action( 'welcome_panel', 'wp_welcome_panel' );





/**
 * Disable the author archive page.
 *
 * @param object $query WordPress query object.
 * @return object $query
 */
function twk_disable_author_archive( $query ) {
	if ( isset( $query->query_vars['author'] ) ) {
		header( 'HTTP/1.0 403 Forbidden' );
		exit;
	}
	return $query;
}
add_action( 'parse_request', 'twk_disable_author_archive' );



/**
 * Delete admin bar
 *
 * @return boolean
 */
function twk_my_function_admin_bar() {
	return false;
}
add_filter( 'show_admin_bar', 'twk_my_function_admin_bar' );




/**
 * Add variable.
 *
 * @param [type] $a_vars .
 * @return $a_vars
 */
function twk_add_query_vars( $a_vars ) {
	$a_vars[] = 'QUERYNAME'; // represents the name of the product category as shown in the URL.

	return $a_vars;
}
// add_filter( 'query_vars', 'twk_add_query_vars' ); // hook add_query_vars function into query_vars.

/**
 * Add rewrite rules.
 *
 * @param [type] $a_rules .
 * @return $a_rules
 */
function twk_add_rewrite_rules( $a_rules ) {
	$a_new_rules = array( 'barristers/expertise/([^/]+)/seniority/([^/]+)?$' => 'index.php?page_id=28&expertise=$matches[1]&seniority=$matches[2]' );

	$a_rules = $a_new_rules + $a_rules;

	return $a_rules;
}
// add_filter( 'rewrite_rules_array', 'twk_add_rewrite_rules' ); // hook add_rewrite_rules function into rewrite_rules_array.





/**
 *  Disable Embeds in WordPress posts.
 *
 * @return void
 */
function twk_my_deregister_scripts() {
	wp_dequeue_script( 'wp-embed' );
}
//add_action( 'wp_footer', 'twk_my_deregister_scripts' );



// GF method: http://www.gravityhelp.com/documentation/gravity-forms/extending-gravity-forms/hooks/filters/gform_init_scripts_footer/ .
add_filter( 'gform_init_scripts_footer', '__return_true' );


/**
 * Solution to move remaining JS from https://bjornjohansen.no/load-gravity-forms-js-in-footer .
 *
 * @param string $content Content.
 * @return string $content
 */
function twk_wrap_gform_cdata_open( $content = '' ) {
	$content = 'document.addEventListener( "DOMContentLoaded", function() { ';

	return $content;
}
add_filter( 'gform_cdata_open', 'twk_wrap_gform_cdata_open' );

/**
 * Solution to move remaining JS from https://bjornjohansen.no/load-gravity-forms-js-in-footer .
 *
 * @param string $content Content.
 * @return string $content
 */
function twk_wrap_gform_cdata_close( $content = '' ) {
	$content = ' }, false );';

	return $content;
}
add_filter( 'gform_cdata_close', 'twk_wrap_gform_cdata_close' );


/**
 * Set the default admin color.
 *
 * @param integer $user_id ID.
 * @return void
 */
function twk_set_default_admin_color( $user_id ) {
	$args = array(
		'ID'          => $user_id,
		'admin_color' => 'twkmedia',
	);
	wp_update_user( $args );
}
add_action( 'user_register', 'twk_set_default_admin_color' );



/**
 * Admin footer modification
 *
 * @return void
 
function remove_footer_admin() {
	echo '<span id="footer-thankyou">Developed by <a href="https://thewebkitchen.co.uk" target="_blank" rel="noopener noreferrer">The Web Kitchen</a></span>';
}
add_filter( 'admin_footer_text', 'remove_footer_admin' );
*/

/**
 * Change wp-logo admin bar menu item.
 *
 * @return void
 */
function twk_remove_wp_links_admin_bar() {
	global $wp_admin_bar;

	// Remove the WordPress links.
	//$wp_admin_bar->remove_menu( 'wp-logo' );
	$wp_admin_bar->remove_menu( 'about' );
	$wp_admin_bar->remove_menu( 'wporg' );
	$wp_admin_bar->remove_menu( 'documentation' );
	$wp_admin_bar->remove_menu( 'support-forums' );
	$wp_admin_bar->remove_menu( 'feedback' );

	$wp_admin_bar->add_menu(
		array(
			'parent' => 'wp-logo',
			'id'     => 'twkmedia',
			'title'  => __( 'The web kitchen' ),
			'href'   => 'https://www.thewebkitchen.co.uk/',
			'meta'   => array(
				'target' => '_blank',
			),
		)
	);

}
add_action( 'wp_before_admin_bar_render', 'twk_remove_wp_links_admin_bar' );








/**
 * Add CPT to the "At a glance" widget.
 *
 * @param array $items
 * @return void
 */
function twk_at_glance_content_add_cpts() {
	$args = array(
		'public'   => true,
		'_builtin' => false,
	);

	$output   = 'object';
	$operator = 'and';

	$post_types = get_post_types( $args, $output, $operator );

	foreach ( $post_types as $post_type ) {
		$num_posts = wp_count_posts( $post_type->name );
		$num       = number_format_i18n( $num_posts->publish );
		$text      = _n( $post_type->labels->singular_name, $post_type->labels->name, intval( $num_posts->publish ) );

		if ( current_user_can( 'edit_posts' ) ) {
			$output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a>';
			echo '<li class="post-count ' . $post_type->name . '-count">' . $output . '</li>';
		}
	}
}
add_action( 'dashboard_glance_items', 'twk_at_glance_content_add_cpts' );



/**
 * Add user info to the $classes array in the backend.
 *
 * @param [type] $classes Space-separated string of class names.
 * @return $classes
 */
function twk_admin_body_class( $classes ) {
	global $current_user;
	$user_role = array_shift( $current_user->roles );

	$user_ID  = $current_user->ID;
	$classes .= ' user-role-' . $user_role . ' user-id-' . $user_ID . ' ';

	return $classes;
}
add_filter( 'admin_body_class', 'twk_admin_body_class' );



function more_post_ajax(){

    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 6;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
    $cat = (isset($_POST['cat'])) ? $_POST['cat'] : '';


    header("Content-Type: text/html");

    $args = array(
        'suppress_filters' => true,
        'post_type' => 'post',
        'posts_per_page' => $ppp,
        'paged'    => $page,
        'post_status' => array('publish'),
        'cat' => $cat,
        // 'post__not_in' => array($post->ID),
    );

    $loop = new WP_Query($args);

    $out = '';
    $deactivate = 'active';

    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();    
        $featimage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' )[0];
        $categories = get_the_category();
        if (empty(get_previous_post_link())) {
                    $deactivate = 'deactivate';
            }
        $has_categories = '';
        if ( ! empty( $categories ) ) {
            $has_categories = $categories[0]->name;
        }    
            
        $out .= '<a class="col-lg-6 col-md-12 col-sm-12 blog-tile-link '. $deactivate .'" href="'.get_the_permalink() .'">
                        <div class="blog-tile" style="background:url('.$featimage.');
                                                    background-position:center;
                                                    background-size:cover;">
                        <span class="post-categories post-meta">'. $has_categories .'</span>
                <span class="post-date post-meta">'. get_the_date( 'd M Y', $post->ID ) .'</span>
                <div class="blog-tile-text">
                <h3>'. get_the_title() .'</h3>
                <p class="post-excerpt">'. get_the_excerpt() .'</p>
                </div>
            </div>
            </a>';

    endwhile;
    endif;
    wp_reset_postdata();
    die($out);
}

function more_video_ajax() {
    
    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 6;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

    header("Content-Type: text/html");

    $args = array(
        'suppress_filters' => true,
        'post_type' => 'video',
        'posts_per_page' => $ppp,
        'paged'    => $page,
        'post_status' => array('publish'),
        // 'post__not_in' => array($post->ID),
    );

    $loop = new WP_Query($args);

    $out = '';
    $deactivate = 'active';

    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();    
        $featimage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' )[0];
        $categories = get_the_category();
        if (empty(get_previous_post_link())) {
                    $deactivate = 'deactivate';
            }
        $has_categories = '';
        if ( ! empty( $categories ) ) {
            $has_categories = $categories[0]->name;
        }
    
        $video_link = get_field( "video_link" );
        $video_desc = get_field( "video_description");
        $template_uri = get_template_directory_uri();
            
        $out .= '<div class="video-tile col-md-6 '. $deactivate .'" style="background:url('.$featimage.');
                                                    background-position:center;
                                                    background-size:cover;">
                 <a class="magnific-video" href="'. $video_link .'"><img class="play-icon" src="'.$template_uri.'/assets/img/play-icon.svg" alt="Play" /></a>      
                 <a class="magnific-video video-link" href="'.$video_link.'"></a>   
                        <div class="video-tile-text">
                        <h3>'. get_the_title() .'</h3>
                        <p class="video-excerpt">'.$video_desc.'</p>
                        </div>
                </div>';

    endwhile;
    endif;
    wp_reset_postdata();
    die($out);
    
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');

add_action('wp_ajax_nopriv_more_video_ajax', 'more_video_ajax');
add_action('wp_ajax_more_video_ajax', 'more_video_ajax');
