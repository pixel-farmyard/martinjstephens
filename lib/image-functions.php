<?php
/**
 * Available custom function for the images.
 *
 * @package twkmedia
 */

/**
 * Get Image URL.
 *
 * @param String $field ACF Custom Field to get the image.
 * @param String $size Size of the image.
 * @param String $post_id ID of the post.  ["ID", "option", "current_ID" (default)].
 * @return String Retrieve an image URL.
 */
function twk_get_image_url( $field, $size, $post_id = null ) {
	if ( ! isset( $post_id ) ) {
		$post_id = get_the_ID();    // current ID as the default.
	}

	$img = get_field( $field, $post_id );

	// If null, we check if it's a sub field.
	if ( $img === null ) {
		$img = get_sub_field( $field, $post_id );
	}

	// The image field returns the URL.
	if ( is_string( $img ) ) {
		return $img;
	}

	// The image field returns an array.
	if ( is_array( $img ) ) {
		if ( $size === 'full' ) {
			return $img['url'];
		} else {
			return $img['sizes'][ $size ];
		}
	}

	// The image field returns an ID.
	$image = wp_get_attachment_image_src( $img, $size )[0];

	return $image;
}


/**
 * Get the image ID.
 *
 * @param String $field
 * @param String $post_id
 * @return void
 */
function twk_get_image_id( $field, $post_id = null ) {
	if ( ! isset( $post_id ) ) {
		$post_id = get_the_ID();    // current ID as the default.
	}

	$img = get_field( $field, $post_id );

	// If null, we check if it's a sub field.
	if ( $img === null ) {
		$img = get_sub_field( $field, $post_id );
	}

	// The image field returns the URL.
	if ( is_string( $img ) ) {
		return attachment_url_to_postid( $img );
	}

	// The image field returns an array.
	if ( is_array( $img ) ) {
		return $img['ID'];
	}

	// The image field returns an ID.
	return $img;
}



/**
 * Get Image HTML.
 *
 * @param String $field ACF Custom Field to get the image.
 * @param String $size Size of the image.
 * @param String $class Class to add to the image.
 * @return String Html for the image.
 */
function twk_get_image( $field, $size, $post_id = null, $class = null ) {
	$img_id   = twk_get_image_id( $field, $post_id );
	$img_url  = wp_get_attachment_image_src( $img_id, $size )[0];
	$alt_text = get_post_meta( $img_id, '_wp_attachment_image_alt', true );

	return '<img src="' . $img_url . '" alt="' . $alt_text . '" class="' . $class . '"/>';
}













/**
 * Insert Image.
 *
 * @param String $field Field to get the image.
 * @param String $size Size of the image.
 * @return array Retrieve an image to represent an attachment.
 */
function insert_image( $field, $size ) {
	$img_id = get_field( $field );
	$image  = wp_get_attachment_image_src( $img_id, $size );

	return $image;
}

/**
 * Insert Image from options page.
 *
 * @param String $field Field to get the image from the options page.
 * @param String $size Size of the image.
 * @return array Retrieve an image to represent an attachment.
 */
function insert_image_options( $field, $size ) {
	$img_id = get_field( $field, 'option' );
	$image  = wp_get_attachment_image_src( $img_id, $size );

	return $image;
}

/**
 * Insert Image from sub field.
 *
 * @param String $field Sub Field to get the image.
 * @param String $size Size of the image.
 * @return array Retrieve an image to represent an attachment.
 */
function insert_sub_image( $field, $size ) {
	$img_id = get_sub_field( $field );
	$image  = wp_get_attachment_image_src( $img_id, $size );

	return $image;
}

/**
 * Insert Image Alt.
 *
 * @param String $field Field to get the image.
 * @param String $size Size of the image.
 * @param String $class Class to add to the image.
 * @return String Html for the image.
 */
function insert_image_alt( $field, $size, $class ) {
	$img_id   = get_field( $field );
	$image    = wp_get_attachment_image_src( $img_id, $size );
	$alt_text = get_post_meta( $img_id, '_wp_attachment_image_alt', true );

	return '<img src="' . $image . '" alt="' . $alt_text . '" class="' . $class . '"/>';
}



function twk_output_featured_image($size, $class){
            $img_size = $size; 
            $image_src = wp_get_attachment_image_src(get_post_thumbnail_id(), $img_size,true); 
            $img_srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id(), $img_size );
            $img_srcset_sizes = wp_get_attachment_image_sizes( get_post_thumbnail_id(), $img_size );
            $img_alt = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
    
            //think if you need custom sizes eg, are any images not full width on mobile
    
    	return '<img src="' . $image_src[0] . '" alt="' . $img_alt[0] . '" srcset="' . $img_srcset . '" sizes="' . $img_srcset_sizes . '" class="' . $class . '"/>';

}

function twk_output_acf_image($image, $size, $class){
            $img_size = $size; 
            $image_src = wp_get_attachment_image_src($image['ID'], $img_size,true); 
            $img_srcset = wp_get_attachment_image_srcset( $image['ID'], $img_size );
            $img_srcset_sizes = wp_get_attachment_image_sizes( $image['ID'], $img_size );
            $img_alt = get_post_meta( $image['ID'], '_wp_attachment_image_alt', true );
    
    	return '<img src="' . $image_src[0] . '" alt="' . $img_alt[0] . '" srcset="' . $img_srcset . '" sizes="' . $img_srcset_sizes . '" class="' . $class . '"/>';

}

function twk_output_id_image($image_id, $size, $class){
            $img_size = $size; 
            $image_src = wp_get_attachment_image_src($image_id, $img_size,true); 
            $img_srcset = wp_get_attachment_image_srcset( $image_id, $img_size );
	       $img_srcset_sizes = wp_get_attachment_image_sizes( $image_id, $img_size );
            $img_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
    
    	return '<img src="' . $image_src[0] . '" alt="' . $img_alt[0] . '" srcset="' . $img_srcset . '" sizes="' . $img_srcset_sizes . '" class="' . $class . '"/>';

}

function twk_output_acf_image_responsive($image, $size, $class){
            $img_size = $size; 
            $image_src = wp_get_attachment_image_src($image['ID'], $img_size,true); 
            $img_srcset = wp_get_attachment_image_srcset( $image['ID'], $img_size );
            $img_srcset_sizes = wp_get_attachment_image_sizes( $image['ID'], $img_size );
            $img_alt = get_post_meta( $image['ID'], '_wp_attachment_image_alt', true );
    
            $img_dimensions = image_get_intermediate_size($image['ID'], 'full');
            $aspect_ratio = ($image_src['2'] / $image_src['1']) * 100;
    
    	return '<div class="' . $class . '"><div class="responsive-img" style="padding-bottom: ' . $aspect_ratio . '%"><img src="' . $image_src[0] . '" alt="' . $img_alt[0] . '" srcset="' . $img_srcset . '" sizes="' . $img_srcset_sizes . '" /></div></div>';

}

// Allow SVG
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

  global $wp_version;
  if ( $wp_version !== '4.7.1' ) {
     return $data;
  }

  $filetype = wp_check_filetype( $filename, $mimes );

  return [
      'ext'             => $filetype['ext'],
      'type'            => $filetype['type'],
      'proper_filename' => $data['proper_filename']
  ];

}, 10, 4 );

function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

function fix_svg() {
  echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
             width: 100% !important;
             height: auto !important;
        }
        </style>';
}
add_action( 'admin_head', 'fix_svg' );

