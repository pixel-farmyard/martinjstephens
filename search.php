<?php
/**
 * The template for displaying search results pages.
 *
 * @package twkmedia
 */

get_header();

?>

<!-- BANNER -->
<div class="banner py-30">
	<div class="container text-center">
		<div class="row justify-content-center">
			<div class="col-md-8 off-screen off-screen--hide off-screen--fade-up">
				<h1 class="banner__title">Search results</h1>

				<div class="banner__intro"><?php echo 'Your search results for "' . sanitize_text_field( get_query_var( 's' ) ) . '"'; ?></div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-8 offset-md-2">

			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					?>

					<article <?php post_class( 'search-result my-5' ); ?> id="post-<?php the_ID(); ?>">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-4">
									<?php
									$image = twk_get_image_url_with_fallback( get_the_ID(), 'large', 'search' );

									if ( $image ) :
										?>
										<img src="<?php echo $image; ?>" alt="Image for <?php echo get_the_title(); ?>">
									<?php else : ?>
										<div class="background-primary h-100 w-100"></div>
									<?php endif; ?>
								</div>

								<div class="col-md-8">
									<a href="<?php the_permalink(); ?>">
										<h2 class="title title--xs"><?php the_title(); ?></h2>
									</a>

									<div class="entry"><?php the_excerpt(); ?></div>
								</div>
							</div>
						</div>
					</article>

				<?php endwhile; ?>


			<?php else : ?>

				<h2 class="title title--medium">No results found.</h2>

			<?php endif; ?>

		</div>
	</div>

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<?php require locate_template( 'tpl/parts/_pagination.php' ); ?>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();
