<?php 

get_header();

?>

		<main class="main">
            <div class="container-fluid">
                <div class="row justify-content-md-center">
                    <div class="col-md-12 the-banner">
                    <h1 class="title title--xl highlighter">Blog</h1>
                    <?php if(get_field('intro')): ?>
                        <p style="max-width:60%; margin: 0 auto; padding:60px 0 0 0; font-weight:bold;"><?php echo(get_field('intro')); ?></p>
                    <?php endif; ?>
                    <div class="blog-cat-nav">Categories: 
                        <?php 
                        $categories = get_categories();
                        foreach($categories as $category) {
                            echo '<a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
                        }?>
                        </div>   

                    <?php if ( has_post_thumbnail() ) { echo '<div class="col-md-12 banner-image" style="background-image: url(' . get_the_post_thumbnail_url(get_the_ID(), 'large' ) . ');"></div>';} else { echo '<div class="col-md-12 banner-spacer"></div>'; }?>
                    </div>
                </div>
                </div>
                
                    
<div id="ajax-posts" class="row col-md-12">
        
        <?php include locate_template( 'tpl/parts/blog-post-grid.php' ); ?>
    
        <div id="more_posts" class="big-link-highlighter" data-type="post">Load More</div>     
        </div>
                
		</main>

		

<?php get_footer(); ?>
