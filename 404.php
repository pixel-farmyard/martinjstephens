<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package twkmedia
 */

get_header();
?>

<?php
if ( get_field( '404_banner_image', 'option' ) ) :
	$image_url = get_field( '404_banner_image', 'option' )['url'];
	?>

	<div class="banner banner--404 img-background img-background--cover" style="background-image: url('<?php echo esc_url( $image_url ); ?>');"></div>
<?php endif; ?>

<main class="main">
	<div class="container my-5">
		<div class="row justify-content-center">
			<div class="col-md-8">

				<?php if ( get_field( '404_alternative_title', 'option' ) ) : ?>
					<h1 class="title title--lg"><?php the_field( '404_alternative_title', 'option' ); ?></h1>
				<?php else : ?>
					<h1 class="title title--lg">Not found <small>(404 error page)</small></h1>
				<?php endif; ?>

				<?php if ( get_field( '404_text', 'option' ) ) : ?>
					<p class="text"><?php echo get_field( '404_text', 'option' ); ?></p>
				<?php endif; ?>

				<?php if ( get_field( '404_link', 'option' ) ) : ?>
					<?php echo twk_compose_acf_link( get_field( '404_link', 'option' ), 'button' ); ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
</main>

<?php
get_footer();
