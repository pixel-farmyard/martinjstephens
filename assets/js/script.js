jQuery(document).ready(function ($) {

    
	modernizer: {
        if (!Modernizr.objectfit) {
            $('.object-fit').each(function () {
                var $container = $(this),
                    imgUrl = $container.find('img').prop('src');
                if (imgUrl) {
                    $container
                        .css('backgroundImage', 'url(' + imgUrl + ')')
                        .addClass('not-compatible');
                }
            });
        }
	}

	fixedheader: {
		$(function () {
			$(window).scroll(function () {
				if ($(window).scrollTop() >= 150) {
					$('.top-nav').addClass('fixed-header');
                    $('#page-wrap').removeClass('header-out');
				}
				else {
                    $('#page-wrap').addClass('header-out');
					// setTimeout(function () { $('.top-nav').removeClass('fixed-header');}, 1000);
				}
			});
		});
	}

	anchorlinks: {
		document.querySelectorAll('a[href^="#"]').forEach(anchor => {
			anchor.addEventListener('click', function (e) {
				e.preventDefault();
		
				document.querySelector(this.getAttribute('href')).scrollIntoView({
					behavior: 'smooth'
				});
			});
		});
	}
    
/*    menuvideo: {
                if  $('#menu-item-470').hasClass( 'current-menu-item' ) {
                    alert('hi');
                    $('#menu-item-470:first-child').css("color","#222");
                   // $('#menu-item-158').removeClass("current_page_parent");
            }
    } */


	search: {
		$('.search-icon').on('click', function (e) {
			$('.searchform').toggleClass('search-open');
			$(this).toggleClass('submit-zindex');
			$('.select-lang').removeClass('transform-height');
			$('#menu-main-menu-top, .lang_dropdown').fadeOut();
			$("#s").focus();
			setTimeout(function () {
				$('.close-search').fadeIn();
			}, 300);
		});
		$('.close-search').on('click', function (e) {
			$(this).fadeOut();
			$('.searchform').removeClass('search-open');
			$('.search-icon').removeClass('submit-zindex');
			$('#menu-main-menu-top, .lang_dropdown').fadeIn();
		});
	}

	mobile: {
		///MOBILE MENU - FOR USE WITH TWK_NAV_WALKER
		/* $('.menu .menu-item-has-children > a').click(function (event) {
			event.preventDefault();
			$(this).parent().children('li .sub-menu-wrap').addClass('transform-none');
		});
		$('.back-menu').click(function () {
			$(this).parent().parent().removeClass('transform-none');
		}); */
	}
    
     postcontent: {
        $('.the-content').children().each(function () {
            $(this).removeAttr('style');
        })
    }

	

	externallinks: {
		$('a[href^="mailto:"]').each(function () {
			$(this).addClass('email-link');
		});	 
		
		$('a:not(.email-link):not([class^="magnific-"])').each(function () {
			var a = new RegExp('/' + window.location.host + '/');
			
			if (!a.test(this.href)) {
				$(this).click(function (event) {
				event.preventDefault();
				window.open(this.href, '_blank');
				});
			}
		});
		pdfs: {
			// Open PDFs in new window
			$(function () {
				$('a[href$=".pdf"]').prop('target', '_blank');
			});
		}
	}

	slider: {
		$('.full-width-slider').slick({
			dots: true,
            arrows: false,
            fade: true,
			autoplay: true,
			autoplaySpeed: 4500,
		});


		/**
		 * Gallery slider with thumbnails navigation.
		 */
		var status = $('.gallery-slider__full-nav .pages');
        var slickElement = $('.gallery-slider__full');

        slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
            //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
            var i = (currentSlide ? currentSlide : 0) + 1;
            status.text(i + ' of ' + slick.slideCount);
		});
		
		$('.gallery-slider__full').slick({
            dots: false,
            arrows: true,
            fade: true,
            asNavFor: '.gallery-slider__thumb',
            appendArrows: '.gallery-slider__full-nav',
        });
        $('.gallery-slider__thumb').slick({
            dots: false,
            arrows: true,
            slidesToShow: 4,
            // centerMode: true,
            focusOnSelect: true,
            asNavFor: '.gallery-slider__full',
        });
	}

	magnific: {
		// IFRAME
		$('.magnific-video').magnificPopup({
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false,

			iframe: {
				markup: '<div class="mfp-iframe-scaler">' +
					'<div class="mfp-close"></div>' +
					'<iframe class="mfp-iframe" frameborder="0" allow="autoplay"></iframe>' +
					'</div>',
				patterns: {
					youtu: {
						index: 'youtu.be',
						id: function( url ) {
						
							// Capture everything after the hostname, excluding possible querystrings.
							var m = url.match( /^.+youtu.be\/([^?]+)/ );
					
							if ( null !== m ) {
								return m[1];
							}
					
							return null;
				
						},
						// Use the captured video ID in an embed URL. 
						// Add/remove querystrings as desired.
						src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0'
					},
					youtube: {
						index: 'youtube.com/',
						id: 'v=',
						src: 'https://www.youtube.com/embed/%id%?autoplay=1'
					}
				}
			}
		});


		// INLINE
		$('.magnific-inline-popup').magnificPopup({
            type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
        });


		// GALLERY SINGLE IMAGE
		$('.magnific-gallery-single-image').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-single',
			image: {
				verticalFit: true,
				titleSrc: function titleSrc(item) {
				return item.el.attr('title');
				}
			},
			gallery: {
				enabled: false
			}
		});          

        // GALLERY IMAGE
        $('.magnific-gallery-image').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true,
                titleSrc: function(item) {
                    return item.el.attr('title');
                }
            },
            gallery:{
                enabled:true
            }
		});
	}

	accordion: {
		if ($('.block--accordion').length) {

			$( '.accordion__content--wrapper' ).css('display', 'none'); // All items closed.
	
			$('.block--accordion').find('.js-accordion-trigger').on('click', function() {

				// Close only the items in this accordion.
				$(this).parent().siblings().find('.js-accordion-trigger').next().slideUp('fast'); // reset
				$(this).parent().siblings().find('.js-accordion-trigger').removeClass('open');    // reset

				// Close items in all accordions on the page.
				//$('.js-accordion-trigger').not(this).next().slideUp('fast'); // reset
				//$('.js-accordion-trigger').not(this).removeClass('open');    // reset
				
				if ($(this).hasClass('open')){
					$(this).next().slideUp('fast');
					$(this).removeClass('open');
				} else {
					$(this).next().slideDown('fast');
					$(this).addClass('open');
				}
				
			});
		}
	}

	/* tabs: {
		const tabs = document.querySelectorAll('[role="tab"]');
		const tabList = document.querySelector('[role="tablist"]');

		if ( tabs ) {

			// Add a click event handler to each tab
			tabs.forEach(tab => {
				tab.addEventListener("click", changeTabs);
			});

			// Enable arrow navigation between tabs in the tab list
			let tabFocus = 0;

			if ( tabList ) {
				tabList.addEventListener("keydown", e => {
					// Move right
					if (e.keyCode === 39 || e.keyCode === 37) {
					tabs[tabFocus].setAttribute("tabindex", -1);
					if (e.keyCode === 39) {
						tabFocus++;
						// If we're at the end, go to the start
						if (tabFocus >= tabs.length) {
						tabFocus = 0;
						}
						// Move left
					} else if (e.keyCode === 37) {
						tabFocus--;
						// If we're at the start, move to the end
						if (tabFocus < 0) {
						tabFocus = tabs.length - 1;
						}
					}
	
					tabs[tabFocus].setAttribute("tabindex", 0);
					tabs[tabFocus].focus();
					}
				});
			}

			function changeTabs(e) {
				console.info('tabs clicked');
				const target = e.target;
				const parent = target.parentNode;
				const grandparent = parent.closest('.tabs');


					console.info(grandparent);
				// Remove all current selected tabs
				parent
					.querySelectorAll('[aria-selected="true"]')
					.forEach(t => t.setAttribute("aria-selected", false));

				// Set this tab as selected
				target.setAttribute("aria-selected", true);

				// Hide all tab panels
				grandparent
					.querySelectorAll('[role="tabpanel"]')
					.forEach(p => p.setAttribute("hidden", true));

				// Show the selected panel
				grandparent.parentNode
					.querySelector(`#${target.getAttribute("aria-controls")}`)
					.removeAttribute("hidden");
			}

		}
	} */

	loadingAnimations: {
		$.fn.isOnScreen = function () {
			var win = $(window);
			var viewport = {
				top: win.scrollTop()
			};
			viewport.bottom = viewport.top + win.height() - 80;
		
			var bounds = this.offset();
			bounds.bottom = bounds.top + this.outerHeight();
		
			return (!(viewport.bottom < bounds.top || viewport.top > bounds.bottom));
		};

		// First load Animation
		$('.off-screen').each(function (index) {
			if ($(this).isOnScreen()) {
				$(this).removeClass('off-screen--hide');
			}
		});

		// Animation on scroll
		$(window).scroll(function () {
			$('.off-screen').each(function (index) {
				if ($(this).isOnScreen()) {
					$(this).removeClass('off-screen--hide');
				}
			});
		});
	}

	tableofcontents: {
		if($('main').is('.table-of-contents-on')){

			$(".the-content h2").each(function(){
				$(this).addClass('subheading');
			})

			$(".the-content h3").each(function(){
				$(this).addClass('subheading');
			})

			$(".the-content h2").each(function(index, value){
				var text = $(this).text();
				var textstripped = text.replace(/\s+/g, '-').toLowerCase();
				$(this).attr('id', index + "-" + textstripped);
			}) 

			$(".the-content h3").each(function(index, value){
				var text = $(this).text();
				var textstripped = text.replace(/\s+/g, '-').toLowerCase();
				$(this).attr('id', index + "-" + textstripped);
			})

			$(".subheading").each(function() {
				var text = $(this).text();
				var anchor = $(this).attr('id');
				$('.table-of-contents').append("<li><a href='#" + anchor + "'>" + text + "</a></li>" );
			})

			$(".table-of-contents-wrap").prepend("<h4>Table of Contents</h4>");


		}
	}

	delay: {
		if (location.hash) {			
			var requested_hash = location.hash.slice(1);
			location.hash = '';
			location.hash = requested_hash
			}
	}
    
    loadmore: {
        var ppp = 6; // Post per page
        var pageNumber = 1;
        var cat = 5;

            function load_posts(){
            pageNumber++;
            
            var type = $("#more_posts").data("type");
            var cat = $("#more_posts").data("category");
                
            if(type === "video") {
                var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_video_ajax';
            } else {
                var str = '&cat=' + cat + '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_post_ajax';
            }
            
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "/wp-admin/admin-ajax.php",
                data: str,
                success: function(data){
                    var $data = $(data);
                    if($data.length){
                        $("#ajax-posts").append($data);
                        $("#more_posts").attr("disabled",false);
                        if(type === "video") {
                        $('.magnific-video').magnificPopup({
			                 type: 'iframe',
			                 mainClass: 'mfp-fade',
			                 removalDelay: 160,
			                 preloader: false,
			fixedContentPos: false,

			iframe: {
				markup: '<div class="mfp-iframe-scaler">' +
					'<div class="mfp-close"></div>' +
					'<iframe class="mfp-iframe" frameborder="0" allow="autoplay"></iframe>' +
					'</div>',
				patterns: {
					youtu: {
						index: 'youtu.be',
						id: function( url ) {
						
							// Capture everything after the hostname, excluding possible querystrings.
							var m = url.match( /^.+youtu.be\/([^?]+)/ );
					
							if ( null !== m ) {
								return m[1];
							}
					
							return null;
				
						},
						// Use the captured video ID in an embed URL. 
						// Add/remove querystrings as desired.
						src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0'
					},
					youtube: {
						index: 'youtube.com/',
						id: 'v=',
						src: 'https://www.youtube.com/embed/%id%?autoplay=1'
					}
				}
			}
		});
        }
                        
            } else{
                $("#more_posts").attr("disabled",true);
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}
        
        function check_for_last(){
            setTimeout(
                function() {
                    if($(".deactivate")[0]) {
                        $('#more_posts').addClass('deactivate');
                    }  }, 1000);
            
                }

            $("#more_posts").on("click",function() { // When btn is pressed.
            $("#more_posts").attr("disabled",true); // Disable the button, temp.
            load_posts();
            check_for_last();

        });
    }
    
});


