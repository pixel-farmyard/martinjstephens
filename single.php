<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package twkmedia
 */

get_header();

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();

$toc_enable = get_field('enable_toc');
?>

    <main class="main blog-post <?php if ($toc_enable['0'] == true) {echo 'table-of-contents-on';} ?>">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-12 the-banner">
                    <h1 class="col-md-12 title title--xl post-title"><?php echo get_the_title(); ?></h1>
                    <?php $introtext = get_field('intro');
                        if ( !empty( $introtext ) ) {echo '<p class="page-intro">' . $introtext . '</p>';} ?>
                    <div class="post-meta-container">
                        <span class="post-date post-meta"><?php echo get_the_date( 'd M Y', $post->ID ) ?></span>&emsp;&bull;&emsp;<span class="post-categories post-meta">
                        <?php 
                $categories = get_the_category();
                    foreach( $categories as $category) {
                    $name = $category->name;
                    $category_link = get_category_link( $category->term_id );
                // wrap in a link with href to $category_link when you can
                echo "<a class='category-tag' href=" . $category_link . ">" . esc_attr( $name) . " </a>";}
                ?>          
                        </span>
                    </div>
                    <?php if ( has_post_thumbnail() ) { echo '<div class="col-md-12 banner-image" style="background-image: url(' . get_the_post_thumbnail_url(get_the_ID(), 'large' ) . ');"></div>';}?>
                    </div>
                    
                </div>
                <div class="row justify-content-md-center">
                    <div class="the-content col-md-8">
                    <?php if ($toc_enable['0'] == true) {echo '<div class="table-of-contents-wrap"><ul class="table-of-contents"></ul></div>';} ?>
                    <?php $content = apply_filters( 'the_content', get_the_content() );
                        echo $content; 
                    ?>
                    </div>
                </div>
            
        </div>
         <div class="mjs-blog-more-container row justify-content-center">
                <a class="big-link-highlighter mjs-blog-more" href="<?php echo get_site_url()?>/blog/" title="<?php the_title_attribute(); ?>">more blogs</a> 
        </div>
        
        <div id="mjs-teasers" class="blog-teasers">
                    
            
     <div class="container-fluid">
         <div class="row justify-content-end">
             <h2 class="title--xl reverse-highlighter">More blogs</h2> 
         </div>
         <div class="row blog-teasers">
         <?php $prev_post_exists = get_previous_post();
                    if ($prev_post_exists) { ?>
            
                    <?php
                        global $post;
                        $current_post = $post; // remember the current post

                        for($i = 1; $i <= 2; $i++){
                        $post = get_previous_post(); // this uses $post->ID
                        setup_postdata($post); 
                    if($post) {  ?>
            
                <a class="teaser teaser-<?php echo $i ?>" href="<?php the_permalink() ?>">
                    <div class="teaser-inner" style="background:url(<?php echo the_post_thumbnail_url( 'large' ); ?>);
                                                            background-position:center;
                                                            background-size:cover;">
                        <span class="post-categories post-meta"><?php $categories = get_the_category();
                                                                if ( ! empty( $categories ) ) {
                                                                echo esc_html( $categories[0]->name );   
                                                            } ?></span>
                        <span class="post-date post-meta"><?php echo get_the_date( 'd M Y', $post->ID ); ?></span>
                        <h3><?php the_title(); ?></h3>
                    </div>
                </a>
            
    
<?php }
    }

$post = $current_post;
?>

</div>

<?php } ?>
            </div>
        </div>
        
 


        
        
		</main>

		<?php
	endwhile;
endif;
?>

<?php
get_footer();

