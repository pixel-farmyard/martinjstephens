<?php
/**
 * Require necessary files
 *
 * @package twkmedia
 */

require_once 'lib/backend-setup.php';
require_once 'lib/redirections.php';
require_once 'lib/wp-api.php';
require_once 'lib/shortcodes.php';
//require_once 'lib/class-better-blockquotes.php';
require_once 'lib/custom-posts-and-taxonomies.php';
require_once 'lib/frontend-functions.php';
require_once 'lib/image-functions.php';
//require_once 'lib/instagram-api-class.php';
//require_once 'lib/instagram-api-call.php';
require_once 'lib/load-jquery.php';
require_once 'lib/load-assets.php';
require_once 'lib/plugin-specifics.php';
require_once 'lib/text-editor.php';
require_once 'lib/twitter-api.php';
require_once 'lib/Mobile_Detect.php';

