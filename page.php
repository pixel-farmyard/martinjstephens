<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>

		<main class="main">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-12 the-banner">
                    <h1 class="title title--xl highlighter"><?php echo get_the_title(); ?></h1>
                    <?php $introtext = get_field('intro');
                        if ( !empty( $introtext ) ) {echo '<p class="page-intro">' . $introtext . '</p>';}
                        if ( has_post_thumbnail() ) { echo '<div class="col-md-12 banner-image" style="background-image: url(' . get_the_post_thumbnail_url(get_the_ID(), 'large' ) . ');"></div>';} 
                        if ( empty($introtext) && empty(has_post_thumbnail()) )  { echo '<div class="col-md-12 banner-spacer"></div>'; }?>
                    </div>
                    
                </div>
                <div class="row justify-content-md-center">
                    <div class="the-content col-md-8">
                    <?php $content = apply_filters( 'the_content', get_the_content() );
                        echo $content; 
                    ?>
                    </div>
                </div>
                
                
                <?php $outro = get_field('outro_text');
                        if ( !empty( $outro ) ) {include locate_template( 'tpl/parts/outro.php' );}
                ?>
                     
        </div>
		</main>

		<?php
	endwhile;
endif;

get_footer();

