<?php 

get_header();

?>

		<main class="main">
            <div class="container-fluid">
                <div class="row justify-content-md-center">
                    <div class="col-md-12 the-banner">
                    <h1 class="title title--xl highlighter">Video</h1>
                    <?php if ( has_post_thumbnail() ) { echo '<div class="col-md-12 banner-image" style="background-image: url(' . get_the_post_thumbnail_url(get_the_ID(), 'large' ) . ');"></div>';} else { echo '<div class="col-md-12 banner-spacer"></div>'; }?>
                    </div>
                    
                </div>
                <div id="ajax-posts" class="row col-md-12">
        <?php
            
                $loop = new WP_Query(
                    array(
                                'post_type' => 'video',
                                'posts_per_page' => 5,
                                'post_status' => array('publish'),
                                )
                            );
                    
                    $i = 1;
                    
                            while ( $loop->have_posts() ) : $loop->the_post();
                    
                            if ($i == 1) { ?>
                    <div class="row col-md-12 first-row">    
                        <div id="featured-video" class="video-tile col-lg-7 col-md-12" style="background:url(<?php echo the_post_thumbnail_url( 'medium' ); ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                            <a class="magnific-video" href="<?php echo get_field( "video_link" ); ?>"><img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" /></a>
                        <a class="magnific-video video-link" href="<?php echo get_field( "video_link" ); ?>"></a>
                                    <div class="video-tile-text">
                                    <h3><?php the_title(); ?></h3>
                                    <p class="video-excerpt"><?php echo get_field( "video_description"); ?></p>
                                    </div>
                        </div>

                    
                    <div class="youtube-box col-lg-5 col-md-12">
                        <h4><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social/mjs-video-youtube.svg" alt="youtube" width="40" height="auto"/>YouTube Channel</h4>
                        <p>All videos are uploaded to my Martin J Stephens YouTube channel. New content is posted straight to YouTube first, so subscribe to my channel for the very latest content.</p>
                        <a class="big-link-highlighter" href="https://www.youtube.com/user/martinjstephens">Subscribe</a>
                    </div>
                </div>
                    
            <?php
                $i++;                             
                } else { ?>
                    <div class="video-tile col-md-6" style="background:url(<?php echo the_post_thumbnail_url( 'medium' ); ?>);
                                                    background-position:center;
                                                    background-size:cover;">
                            <a class="magnific-video" href="<?php echo get_field( "video_link" ); ?>"><img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-icon.svg" alt="Play" /></a>
                            <a class="magnific-video video-link" href="<?php echo get_field( "video_link" ); ?>"></a>
                                    <div class="video-tile-text">
                                    <h3><?php the_title(); ?></h3>
                                    <p class="video-excerpt"><?php echo get_field( "video_description"); ?></p>
                                    </div>
                    </div>
                    
                    
                        <?php    
                       $i++;  
                       } wp_reset_postdata(); ?>
                   
                    <?php endwhile; ?>
                
                    <div id="more_posts" class="big-link-highlighter" data-type="video">Load More</div>         
                
                </div>
            
        </div>
		</main>

		

<?php get_footer(); ?>