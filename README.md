# TWK Base Theme

## Changelog

**V3**

	-------- 2020-11 --------

	* Flexible strip loop in separate file with section tag included
	* Add "split list" format
	* Format "intro" directly on p tag
	* Style for custom list dot color
	* Changes on "base" clone ACF field:
    	* margin as select to add no margins
    	* add paddings
    	* add "hidden" block_custom_id
  	* Remove WYSIWYG Editor flexible strip to merge it into Columns Text.
  	* Modifiers.scss: Add responsive md and lg
  	* Add font-display: swap;

	-------- 2020-09 --------

	* Filter to disable WP API endpoints.

	-------- 2020-06 --------

	* Banner
	* Add Mobile_Detect.php library
	* CSS Overlays utilities

	-------- 2020-05 --------

	* WP Menu conditinal show with no fallback
	* Update cookies notification to use JS and not &_COOKIE
	* Change GA for GTM
    	* Update conditional to show: twkmedia.eu to twkmedia.com
  	* Add Notifications Popup
    	* With "expiry_time"
    	* and "marketing" cookie

	-------- 2020-04 --------

	* Add acf columns plugin
	* Banner ACF fields

	-------- 2020-03 --------

	* ACF fields
	* Update margin radio values to be sm, md and lg.
		* SASS: now available my-sm, my-md...
	* Options page: Images (for fallback images), 404 (populate with fields)
	* Add default 404 error page
	* With ACF fields
	* Cookies notifications
	* Move to options page "notifications"
	* Fix "reset"
	* Footer - Social Media links.
	* Add instagram
	* Add ACF fields
	* Min styling
	* Footer - Legal Menu
	* Addition of twk_compose_acf_link function
	* Add "Edit page" link
	* Update WP Rocket function to allow Editors to clear cache
	* TinyMCE - fix: add the _format select_ by default on the ACF pro TinyMCE editor
	* SASS
	* Move font-size and font-family from html to body so it won't get override by BS reboot


	-------- 2020-02 --------

	* ACF sync fields
		* Cookies bar updated
	* Hide editor on pages and add a default WYSIWYG block (acf flexible content)
	* TinyMCE
		* Restrict tinymce headers options (p, h2, h3, h4)
		* Remove leftover function from tinymce buttons
		* Custom toolbars. Remove font family, font size... add dropdown. [also done for acf editor]
	* Delete js/min scripts
	* Remove prepros file
	* New folder img/admin to save admin related images/icons

	* PHP
		* Create folder "blocks" to have all the blocks - acf flexible content.
		* Add credit on footer: "Design by The Web Kitchen".
	* SASS
		* Rename "block" folder to "parts" - Same name as its PHP version
		* Create folder "blocks" for the acf flexible content, blocks.
		* New admin-styles file
		* Update modifiers file and remove it from editor-styles
		* Colors.scss - Remove shadow mixin and change from color-property to property-color
		* Update titles.scss file
		* Remove _accordian.scss part, now in blocks.
	* JS
		* Move calendar code to its own file.
		* Anchor link: updated it to "scroll-to" instead "news-slider__nav a".
		* Slick slider default name "full-width-slider".
		* Add Magnific Popup "inline-popup".
		* Move "onLoad" label to a $(window).on('load') function instead of a double $(document).ready
		* Update loading animations to make it work on firefox by default.

**V2**

    * Added in pseudo-element mixin
    * Added in aspect ratio mixin
    * Added in vertical centering mixin
    * Added in list beautifying mixin
    * Added in font size mixin
    * Added $primary colour variable
    * Added $secondary colour variable
    * Added new template for titles
    * Moved Twitter API to its own file
    * Moved all non-scripts.js files to /js/vendor folder and enqueued those relevant
    * Added @imports for all sass files and commented them out
    * Moved search-form.php to tpl/parts
    * Deleted unnecessary tpl/parts - banner-short and child-page-list
    * Added in banner.php and added variable structure. Added in variable examples to page.php and single.php as well as tpls
    * Tidied content.scss into pagination, search and img.scss - added background utility classes
    * Commented sass files where appropriate for organisation
    * Beautified the whole thing so it's easier to read
    * Added img folder
    * Made the landing page look a bit better
    * Added twk_nav_walker to /lib - adds back button and title to sub-menus for sliding mobile navigation
    * Tidied scripts.js
    * Added scripts for:
      * Opening external links and PDFs in new tab
      * Fixed Header
      * Mobile menu - for use with nav walker
      * Magnific init for videos
